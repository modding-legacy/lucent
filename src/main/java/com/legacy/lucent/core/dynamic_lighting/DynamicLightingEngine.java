package com.legacy.lucent.core.dynamic_lighting;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.legacy.lucent.api.EntityBrightness;
import com.legacy.lucent.api.EntityBrightnessMap;
import com.legacy.lucent.api.VecTransformer;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.registry.EntityLightSourcePosRegistry;
import com.legacy.lucent.core.LucentClient;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.capability.IEntityLightingData;
import com.legacy.lucent.core.dynamic_lighting.LightData.SourcePos;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.SectionPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Creates dynamic lighting by telling the render engine to display different
 * light levels at specific positions.
 * 
 * Note: This is an API. Please do not just copy the code into your own mod. The
 * API exists for a reason.
 * https://maven.moddinglegacy.com/service/rest/repository/browse/maven-public/com/legacy/lucent/
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public final class DynamicLightingEngine implements Runnable
{
	private static final Thread INSTANCE = new Thread(new DynamicLightingEngine());
	private static Map<BlockPos, LightData> lightData = new HashMap<>();
	private static Map<BlockPos, LightData> forcedLightData = new HashMap<>();
	private static Set<SectionPos> sectionsToUpdate = new HashSet<>();

	private DynamicLightingEngine()
	{

	}

	private static Minecraft MC()
	{
		return Minecraft.getInstance();
	}

	@SuppressWarnings("resource")
	private static Level level()
	{
		return MC().level;
	}

	@SuppressWarnings("resource")
	private static Player player()
	{
		return MC().player;
	}

	private static Vec3 playerPos()
	{
		return player().position();
	}

	/**
	 * Checks if a light source exists at the given position
	 * 
	 * @param pos
	 * @return If a light source exists
	 */
	public static boolean hasLightSource(BlockPos pos)
	{
		return lightData.containsKey(pos) || forcedLightData.containsKey(pos);
	}

	/**
	 * Gets the stored light level at the given position
	 * 
	 * @param pos
	 * @return The light source. Null if not present
	 */
	@Nullable
	public static LightData getLightSource(BlockPos pos)
	{
		LightData data = lightData.get(pos);
		if (data == null)
			data = forcedLightData.get(pos);

		return data;
	}

	/**
	 * Checks if a chunk section is queued for a render update at the given position
	 * 
	 * @param pos
	 * @return If the chunk section is queued for update
	 */
	public static boolean sectionNeedsUpdate(BlockPos pos)
	{
		return sectionNeedsUpdate(SectionPos.of(pos));
	}

	/**
	 * Checks if a chunk section is queued for a render update at the given position
	 * 
	 * @param sectionPos
	 * @return If the chunk section is queued for update
	 */
	public static boolean sectionNeedsUpdate(SectionPos sectionPos)
	{
		return sectionsToUpdate.contains(sectionPos);
	}

	/**
	 * Calculates the light value at the given position and returns the result
	 * 
	 * @param level
	 * @param state
	 *            The block state at pos
	 * @param pos
	 * @param lightData
	 *            The light data at pos
	 * @return The packed light value
	 */
	public static int calcLight(BlockAndTintGetter level, BlockState state, BlockPos pos, LightData lightData)
	{
		int skyLight = level.getBrightness(LightLayer.SKY, pos);
		float blockLight = calcBlockLight(level, pos, lightData);
		int blockStateLight = state.getLightEmission(level, pos);
		if (blockLight < blockStateLight)
			blockLight = blockStateLight;

		return LucentClient.packLight(blockLight, skyLight);
	}

	/**
	 * Calculates the block light at the given position and returns the result.
	 * Attempts to smoothly blend light levels if enabled in the config
	 * 
	 * @param level
	 * @param pos
	 * @param lightData
	 *            The light data at pos
	 * @return The block light value
	 */
	public static float calcBlockLight(BlockAndTintGetter level, BlockPos pos, LightData lightData)
	{
		int levelBlockLight = level.getBrightness(LightLayer.BLOCK, pos);
		if (lightData != null)
		{
			// If light not cached, calculate and store it
			if (lightData.calculatedBlockLight == -1.0F)
			{
				float blockLight = lightData.lightLevel;

				if (LucentConfig.CLIENT.smoothBlending.get())
				{
					// Get the brightest light based on the sources
					List<LightData.SourcePos> sourcePoses = lightData.sourcePoses;
					float maxBlockLight = 0;
					for (int i = sourcePoses.size() - 1; i > -1; i--)
					{
						LightData.SourcePos sourcePos = sourcePoses.get(i);
						if (sourcePos != null)
						{
							LightData parentData = getLightSource(BlockPos.containing(sourcePos.x, sourcePos.y, sourcePos.z));
							if (parentData != null)
							{
								int parentLight = parentData.lightLevel;
								float actualDist = calcDist(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, sourcePos.x, sourcePos.y, sourcePos.z);
								int dist = calcDist(pos.getX(), pos.getY(), pos.getZ(), (int) Math.floor(sourcePos.x), (int) Math.floor(sourcePos.y), (int) Math.floor(sourcePos.z));
								int wrappedDist = parentLight - (int) blockLight;

								float light = parentLight - actualDist;
								if (wrappedDist > dist)
									light -= (wrappedDist - dist);

								maxBlockLight = Math.max(maxBlockLight, Mth.clamp(light, 0.0F, 15.0F));
							}
						}
					}

					blockLight = maxBlockLight;
				}

				lightData.calculatedBlockLight = blockLight;
			}

			// Return the cached light level
			return Math.max(lightData.calculatedBlockLight, levelBlockLight);
		}

		return levelBlockLight;
	}

	/**
	 * Manually adds a light source to the specified position and queues a light
	 * update
	 * 
	 * @param pos
	 *            The position to add light to
	 * @param state
	 */
	public static void blockChanged(BlockPos pos, BlockState state)
	{
		if (state.isSolid() || level() == null)
			return;

		// Get the brightest adjacent dyn light
		LightData brightestLight = LightData.NONE;
		for (Direction dir : Direction.values())
		{
			BlockPos relativePos = pos.relative(dir);

			if (canLightPass(pos, relativePos, dir))
			{
				LightData offsetLight = getLightSource(relativePos);
				if (offsetLight != null && offsetLight.lightLevel > brightestLight.lightLevel)
				{
					brightestLight = offsetLight;
				}
			}
		}

		// Remove existing
		LightData data = getLightSource(pos);
		if (data != null)
		{
			SourcePos sourcePos = data.sourcePoses.get(0);
			if (!pos.equals(BlockPos.containing(sourcePos.x, sourcePos.y, sourcePos.z)) && brightestLight.lightLevel - 1 != data.lightLevel)
			{
				lightData.remove(pos);
				forcedLightData.remove(pos);
			}
		}

		// Add a new light if the brightest light is > 0
		if (brightestLight.lightLevel > 0)
		{
			LightData inserted = new LightData(brightestLight.lightLevel - 1, brightestLight.sourcePoses.get(0));
			forcedLightData.put(pos, inserted);
			setDirty(SectionPos.of(pos));
		}
	}

	/**
	 * Removes all manually added light sources if a normal light source exists at
	 * the location
	 */
	public static void clearForcedLights()
	{
		if (!forcedLightData.isEmpty())
		{
			try
			{
				boolean needsCleared = true;
				for (BlockPos pos : forcedLightData.keySet().stream().toList())
				{
					if (lightData.containsKey(pos))
					{
						forcedLightData.remove(pos);
					}
					else
					{
						for (Direction dir : Direction.values())
						{
							BlockPos offset = pos.relative(dir);
							if (hasLightSource(offset))
							{
								forcedLightData.remove(pos);
								break;
							}
						}
					}

					needsCleared = false;
				}

				if (needsCleared)
				{
					forcedLightData = new HashMap<>();
				}
			}
			catch (Throwable t)
			{
				// Doing this due to potential threading conflics
			}
		}
	}

	private static boolean inDeepDark = false;
	private static int deepDarkTransitionTime = 20;
	private static int deepDarkTransition = deepDarkTransitionTime;
	private static float deepDarkScale = 0.0F;

	public static void tickDeepDark()
	{
		Player player = player();
		if (player != null)
		{
			Level level = level();
			if (level != null)
			{
				if (player.tickCount % 20 == 0)
					inDeepDark = level.getBiome(BlockPos.containing(playerPos())).is(Biomes.DEEP_DARK);

				if (inDeepDark)
				{
					if (deepDarkTransition > 0)
						deepDarkTransition--;
				}
				else if (deepDarkTransition < deepDarkTransitionTime)
					deepDarkTransition++;

				float ddScale = LucentConfig.CLIENT.deepDarkScaling.get();
				deepDarkScale = (1.0F - ddScale) * (deepDarkTransition / (float) deepDarkTransitionTime) + ddScale;
			}
		}
	}

	/**
	 * Starts the dynamic lighting manager instance
	 */
	public static void start()
	{
		if (!INSTANCE.isAlive())
		{
			LucentMod.LOGGER.info("Starting dynamic lighting engine");
			INSTANCE.start();
		}
		else
		{
			LucentMod.LOGGER.info("Attempted to start the dynamic lighting engine, but it was already running.");
		}
	}

	/**
	 * Runs the thread, ticking the lighting engine
	 */
	@Override
	public void run()
	{
		long totalTime = 0;
		int runs = 0;
		Minecraft mc = MC();
		while (true)
		{
			try
			{
				if (mc.level != null && mc.player != null)
				{
					long startTime = System.currentTimeMillis();
					tick();
					long time = System.currentTimeMillis() - startTime;
					int refreshRate = LucentConfig.CLIENT.lightRefreshRate.get();

					// Log refresh rate if enabled
					if (LucentConfig.CLIENT.logLightCalculationTime.get())
					{
						totalTime += time;
						runs++;
						if (runs >= 100)
						{
							long avTime = totalTime / runs;
							LucentMod.LOGGER.info("Light calculations took {}ms with a {}ms refresh rate", avTime, refreshRate);
							if (avTime > refreshRate + 2)
								LucentMod.LOGGER.info("{} is the fastest refresh rate your computer can handle in this environment with the current settings", 1000 / avTime);
							totalTime = 0;
							runs = 0;
						}
					}

					// Sleep for the remaining time of the refresh rate
					long sleepTime = refreshRate - time;
					Thread.sleep(sleepTime < 0 ? 0 : sleepTime);
				}
				else
				{
					// Waiting for the level and player to not be null.
					Thread.sleep(1000);
				}
			}
			catch (Throwable t)
			{
				if (LucentConfig.CLIENT.logDynamicLightingErrors.get())
				{
					t.printStackTrace();
					LucentMod.LOGGER.printStacktrace("Dynamic Lighting Engine Error", t);
				}
				EntityBrightness.activeEntity = null;
			}
		}
	}

	/**
	 * Calculates and updates lighting. Assumes both the client level and player are
	 * not null
	 */
	private static void tick()
	{
		// Queue all existing light sources for removal
		lightData.entrySet().forEach(entry -> entry.getValue().shouldStay = false);

		// Make all entities that should glow add light sources
		Map<BlockPos, LightData> newLightData = new HashMap<>(lightData);

		Player player = player();
		List<Entity> entitiesInRange = level().getEntitiesOfClass(Entity.class, AABB.unitCubeFromLowerCorner(playerPos()).inflate(LucentConfig.CLIENT.maxVisibleDistance.get()));
		if (!entitiesInRange.contains(player))
			entitiesInRange.add(player);

		entitiesInRange.forEach(e ->
		{
			EntityBrightnessMap sources = getEntityLightLevel(e);
			for (var s : sources.getAll().entrySet())
			{
				int lightLevel = s.getValue();
				if (lightLevel > 0)
				{
					Vec3 entityPos = s.getKey().applyForEntity(e);
					addLights(newLightData, BlockPos.containing(entityPos), lightLevel, new LightData.SourcePos(entityPos.x, entityPos.y, entityPos.z));
				}
			}
		});

		newLightData.entrySet().forEach(entry ->
		{
			LightData oldData = lightData.get(entry.getKey());
			if (oldData != null)
			{
				LightData newData = entry.getValue();
				if (oldData.equals(newData))
					newData.dirty = false;
			}
		});

		lightData = newLightData;

		sectionsToUpdate.clear();

		if (!lightData.isEmpty())
		{
			// Update all light sources
			lightData.entrySet().stream().filter(entry -> entry.getValue().dirty || !entry.getValue().shouldStay).forEach(entry ->
			{
				BlockPos pos = entry.getKey();

				SectionPos section = SectionPos.of(pos);
				sectionsToUpdate.add(section);

				int x = pos.getX() & 15;
				int y = pos.getY() & 15;
				int z = pos.getZ() & 15;
				if (x == 0)
					sectionsToUpdate.add(SectionPos.of(pos.west()));
				else if (x == 15)
					sectionsToUpdate.add(SectionPos.of(pos.east()));

				if (y == 0)
					sectionsToUpdate.add(SectionPos.of(pos.below()));
				else if (y == 15)
					sectionsToUpdate.add(SectionPos.of(pos.above()));

				if (z == 0)
					sectionsToUpdate.add(SectionPos.of(pos.north()));
				else if (z == 15)
					sectionsToUpdate.add(SectionPos.of(pos.south()));
			});

			sectionsToUpdate.stream().forEach(DynamicLightingEngine::setDirty);
		}

		// Remove old light sources
		lightData.entrySet().removeIf(entry -> !entry.getValue().shouldStay);

		// Clear forced lights
		forcedLightData = new HashMap<>();
	}

	/**
	 * Gets the light level to be emitted by the entity passed
	 * 
	 * @param entity
	 * @return The light level of the entity. 0 if it shouldn't emit light
	 * 
	 */
	private static EntityBrightnessMap getEntityLightLevel(Entity entity)
	{
		Player player = player();
		EntityBrightness.activeEntity = entity;
		// Single point brightness
		EntityBrightness brightness = new EntityBrightness();
		for (ILucentPlugin plugin : LucentClient.getPlugins())
			plugin.getEntityLightLevel(brightness);
		int l = brightness.getLightLevel();
		// Store the light emitted by the entity to prevent flickering
		((IEntityLightingData) entity).lucent$setBrightness(l);

		// Deep dark
		if (l > 0)
		{
			l = Math.round(deepDarkScale * l);
		}

		// Multi point brightness
		EntityBrightnessMap brightnessMap = new EntityBrightnessMap();
		brightnessMap.add(new VecTransformer.Direct(EntityLightSourcePosRegistry.get(entity)), l);
		for (ILucentPlugin plugin : LucentClient.getPlugins())
			plugin.getAdditionalEntityLightLevels(brightnessMap);

		brightnessMap.getAll().entrySet().removeIf(e ->
		{
			boolean visible = LucentConfig.CLIENT.seeThroughWalls.get();
			if (!visible)
			{
				Vec3 pos = e.getKey().applyForEntity(entity);
				visible = level().clip(new ClipContext(playerPos().add(0, player.getEyeHeight(), 0), pos, ClipContext.Block.VISUAL, ClipContext.Fluid.NONE, entity)).getType() == HitResult.Type.MISS;

				if (!visible && playerPos().distanceTo(entity.position()) < 24 || level().getBrightness(LightLayer.SKY, BlockPos.containing(pos)) > 8)
				{
					visible = true;
				}
			}
			return !visible;
		});

		EntityBrightness.activeEntity = null;
		return brightnessMap;
	}

	/**
	 * Adds light sources around pos
	 * 
	 * @param newLightData
	 *            The data map to reference for light calculations
	 * @param pos
	 *            The start position to spread the light from
	 * @param lightLevel
	 *            The amount of light to emit
	 * @param sourcePos
	 *            The exact position of the entity that the light came from. Used
	 *            for blending the light smoothly
	 */
	private static void addLights(Map<BlockPos, LightData> newLightData, BlockPos pos, int lightLevel, LightData.SourcePos sourcePos)
	{
		if (addLightSource(newLightData, pos, lightLevel, sourcePos))
		{
			Map<BlockPos, Integer> positions = new HashMap<>();
			positions.put(pos, lightLevel);

			//long start = System.nanoTime();
			for (int l = lightLevel - 1; l > (LucentConfig.CLIENT.smoothBlending.get() ? -1 : 0); l--)
			{
				final int lightFilter = l + 1;
				for (BlockPos currentPos : positions.entrySet().stream().filter(e -> e.getValue() == lightFilter).map(Map.Entry::getKey).toArray(BlockPos[]::new))
				{
					for (Direction dir : Direction.values())
					{
						BlockPos relativePos = currentPos.relative(dir);
						if (!positions.containsKey(relativePos) && canLightPass(currentPos, relativePos, dir) && addLightSource(newLightData, relativePos, l, sourcePos))
						{
							positions.put(relativePos, l);
						}
					}
				}
			}
			//System.out.println("took: " + ((System.nanoTime() - start) / 100000.0)+ "ms");
		}
	}

	/**
	 * Adds a light source to the map of sources. If a light source already exists
	 * at pos, the highest light level source is prioritized
	 * 
	 * @param newLightData
	 *            The data map to reference for light calculations
	 * @param pos
	 *            The position to add a source to
	 * @param lightLevel
	 *            The level of light to be emitted by the source
	 * @param sourcePos
	 *            The exact position of the entity that the light came from. Used
	 *            for blending the light smoothly
	 * @return True if a light source was placed
	 */
	private static boolean addLightSource(Map<BlockPos, LightData> newLightData, BlockPos pos, int lightLevel, LightData.SourcePos sourcePos)
	{
		LightData oldData = newLightData.get(pos);
		if (oldData != null && oldData.shouldStay)
		{
			if (LucentConfig.CLIENT.smoothBlending.get())
			{
				if (oldData.lightLevel > lightLevel + 1)
					return false;

				if (lightLevel > oldData.lightLevel)
					oldData.lightLevel = lightLevel;

				oldData.sourcePoses.add(sourcePos);
				return true;
			}
			else if (lightLevel > oldData.lightLevel)
			{
				newLightData.put(pos, new LightData(lightLevel, sourcePos));
				forcedLightData.remove(pos);
				return true;
			}
		}
		else
		{
			newLightData.put(pos, new LightData(lightLevel, sourcePos));
			forcedLightData.remove(pos);
			return true;
		}
		return false;
	}

	/**
	 * Internal method to get the city block distance between two points in 3D space
	 * 
	 * @param x1
	 * @param y1
	 * @param z1
	 * @param x2
	 * @param y2
	 * @param z2
	 * @return A city block distance
	 */
	private static float calcDist(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		return (float) (Math.abs(x1 - x2) + Math.abs(y1 - y2) + Math.abs(z1 - z2));
	}

	/**
	 * Internal method to get the city block distance between two points in 3D space
	 * 
	 * @param x1
	 * @param y1
	 * @param z1
	 * @param x2
	 * @param y2
	 * @param z2
	 * @return A city block distance
	 */
	private static int calcDist(int x1, int y1, int z1, int x2, int y2, int z2)
	{
		return (int) (Math.abs(x1 - x2) + Math.abs(y1 - y2) + Math.abs(z1 - z2));
	}

	/**
	 * Internal helper to get the voxel shape of a block's side
	 * 
	 * @param state
	 *            The block state to check
	 * @param pos
	 *            The position of state
	 * @param dir
	 *            The face to get
	 * @return The voxel shape of the face passed
	 */
	private static VoxelShape getShape(BlockState state, BlockPos pos, Direction dir)
	{
		Level level = level();
		return (state.getLightBlock(level, pos) < 15 && !state.canOcclude()) || !state.isSolid() ? Shapes.empty() : state.getFaceOcclusionShape(level, pos, dir);
	}

	/**
	 * Checks if light can pass from the block at currentPos to the block at
	 * relativePos traveling in the direction of dir.
	 * 
	 * @param currentPos
	 *            The start position
	 * @param relativePos
	 *            The target position
	 * @param dir
	 *            The direction to travel
	 * @return If light can pass through the block
	 */
	private static boolean canLightPass(BlockPos currentPos, BlockPos relativePos, Direction dir)
	{
		Level level = level();
		return !Shapes.faceShapeOccludes(getShape(level.getBlockState(currentPos), currentPos, dir), getShape(level.getBlockState(relativePos), relativePos, dir.getOpposite()));
	}

	/**
	 * Marks the chunk at pos to be re-rendered
	 * 
	 * @param sectionPos
	 */
	private static void setDirty(SectionPos sectionPos)
	{
		// Allow mods to prevent this logic from happening
		if (LucentClient.pluginManager().post(p -> p.markSectionDirty(sectionPos)))
			return;
		LucentClient.scheduleSetDirty(sectionPos);
	}

	public static void clearAllData()
	{
		lightData.clear();
		forcedLightData.clear();
		sectionsToUpdate.clear();

		lightData = new HashMap<>();
		forcedLightData = new HashMap<>();
		sectionsToUpdate = new HashSet<>();
	}
}