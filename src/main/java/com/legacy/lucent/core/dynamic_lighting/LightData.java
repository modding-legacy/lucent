package com.legacy.lucent.core.dynamic_lighting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Stores data about a light source to determine how to render it and when to
 * remove it.
 * 
 * Note: This is an API. Please do not just copy the code into your own mod. The
 * API exists for a reason.
 * https://maven.moddinglegacy.com/service/rest/repository/browse/maven-public/com/legacy/lucent/
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class LightData
{
	protected static final LightData NONE = new LightData(0, new LightData.SourcePos(0, 0, 0));
	protected boolean shouldStay = true;
	protected boolean dirty = true;
	protected int lightLevel;
	protected float calculatedBlockLight = -1.0F;
	protected final List<SourcePos> sourcePoses;

	/**
	 * 
	 * @param lightLevel
	 *            The light level of this source
	 * @param sourcePos
	 *            The exact position of the entity that the light came from. Used
	 *            for blending the light smoothly
	 */
	protected LightData(int lightLevel, SourcePos sourcePos)
	{
		this(lightLevel, new ArrayList<>(Arrays.asList(sourcePos)));
	}

	/**
	 * 
	 * @param lightLevel
	 *            The light level of this source
	 * @param sourcePoses
	 *            The exact positions of the entities that the light came from. Used
	 *            for blending the light smoothly
	 */
	protected LightData(int lightLevel, List<SourcePos> sourcePoses)
	{
		this.lightLevel = lightLevel;
		this.sourcePoses = sourcePoses;
	}

	/**
	 * @return The light level at this position, not taking account for distance
	 *         from source
	 */
	public int rawLight()
	{
		return lightLevel;
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof LightData l && this.equals(l);
	}

	/**
	 * Compares the light data passed with this instace
	 * 
	 * @param lightData
	 *            The object to compare
	 * @return True if lightLevel and sourcePos are both equal
	 */
	public boolean equals(LightData lightData)
	{
		return lightLevel == lightData.lightLevel && this.sourcePosEquals(lightData.sourcePoses);
	}

	/**
	 * @param sourcePoses
	 * @return True if data in sourcePoses matches this instance's sourcePoses
	 */
	protected boolean sourcePosEquals(List<SourcePos> sourcePoses)
	{
		int size = this.sourcePoses.size();
		if (size == sourcePoses.size())
		{
			if (size == 0)
			{
				return true;
			}
			else if (size == 1)
			{
				return sourcePoses.get(0).equals(this.sourcePoses.get(0));
			}
			else
			{
				for (int i = 0; i < size; i++)
				{
					if (!sourcePoses.contains(this.sourcePoses.get(i)))
						return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * A basic position holder for light data.
	 * 
	 * @author Silver_David
	 *
	 */
	public static class SourcePos
	{
		protected final double x, y, z;

		protected SourcePos(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		@Override
		public boolean equals(Object obj)
		{
			return obj instanceof SourcePos s && this.equals(s);
		}

		public boolean equals(SourcePos pos)
		{
			return x == pos.x && y == pos.y && z == pos.z;
		}
	}
}