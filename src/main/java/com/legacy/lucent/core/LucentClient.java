package com.legacy.lucent.core;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.LightLevelProviderRegistry;
import com.legacy.lucent.core.data.managers.LucentAssets;
import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;

import net.minecraft.client.Minecraft;
import net.minecraft.core.SectionPos;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.neoforged.neoforge.client.event.ClientPlayerNetworkEvent;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.TickEvent.ClientTickEvent;
import net.neoforged.neoforge.event.TickEvent.Phase;
import net.neoforged.neoforge.event.TickEvent.RenderTickEvent;

public class LucentClient
{
	private static PluginManager<ILucentPlugin> plugins = null;

	public static void init(IEventBus modBus)
	{
		plugins = new PluginManager<>(ILucentPlugin.class, LucentPlugin.class, LucentMod.LOGGER::getLogger, () -> LucentMod.MODID, modBus);
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, LucentConfig.CLIENT_SPEC);

		modBus.addListener(LucentClient::loadComplete);
		modBus.addListener(LucentAssets::registerListeners);

		IEventBus forgeBus = NeoForge.EVENT_BUS;
		forgeBus.register(LucentClient.class);
	}

	/**
	 * Grants access to all lucent plugins.
	 * 
	 * @return The plugin list
	 */
	public static List<ILucentPlugin> getPlugins()
	{
		return plugins.get();
	}

	public static PluginManager<ILucentPlugin> pluginManager()
	{
		return plugins;
	}

	protected static void loadComplete(final FMLLoadCompleteEvent event)
	{
		DynamicLightingEngine.start();
		event.enqueueWork(() ->
		{
			LightLevelProviderRegistry provRegistry = new LightLevelProviderRegistry();
			plugins.forEach(p -> p.registerLightLevelProviderTypes(provRegistry));
		});
	}

	@SubscribeEvent
	protected static void onLogin(final ClientPlayerNetworkEvent.LoggingIn event)
	{
		LucentRegistry.registerData();
		LucentConfig.detectEnvironment();
		synchronized (LucentClient.class)
		{
			toMarkDirty = new HashSet<>();
		}
	}

	@SubscribeEvent
	protected static void clientTick(final ClientTickEvent event)
	{
		if (event.phase == Phase.END)
		{
			DynamicLightingEngine.clearForcedLights();
			DynamicLightingEngine.tickDeepDark();
		}
	}

	@SubscribeEvent
	protected static void renderTick(final RenderTickEvent event)
	{
		if (event.phase == Phase.START && Minecraft.getInstance().level != null)
		{
			if (!toMarkDirty.isEmpty())
			{
				synchronized (LucentClient.class)
				{
					SectionPos[] poses = toMarkDirty.toArray(SectionPos[]::new);
					toMarkDirty = new HashSet<>();
					for (SectionPos section : poses)
					{
						setDirty(section);
					}
				}
			}
		}
	}

	public static void setDirty(SectionPos section)
	{
		Minecraft.getInstance().levelRenderer.setSectionDirty(section.getX(), section.getY(), section.getZ());
		plugins.forEach(p -> p.onSectionMarkedDirty(section));
	}

	/**
	 * Collection of sections to mark dirty on client tick. This is done here to
	 * prevent marking chunks as dirty on the dynlights thread
	 */
	private static Set<SectionPos> toMarkDirty = new HashSet<>();

	/**
	 * Marks the chunk section as needing an update. This can be called from a threaded context.
	 */
	public static void scheduleSetDirty(SectionPos section)
	{
		synchronized (LucentClient.class)
		{
			toMarkDirty.add(section);
		}
	}
	
	public static int packLight(float blockLight, int skyLight)
	{
		// Vanilla handling: blockLight << 4 | skyLight << 20;
		return (int) (blockLight * 16.0F) | (skyLight << 20);
	}
}
