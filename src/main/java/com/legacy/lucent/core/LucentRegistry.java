package com.legacy.lucent.core;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

import com.legacy.lucent.api.registry.ArmorTrimLightingRegistry;
import com.legacy.lucent.api.registry.BlockTextureLightingRegistry;
import com.legacy.lucent.api.registry.EntityLightSourcePosRegistry;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.data.managers.LucentAssets;

import net.minecraft.ResourceLocationException;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.Item;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

/**
 * Internal methods to handle registering data.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class LucentRegistry
{
	public static Source currentSource = Source.UNKNOWN;
	public static String sourceName = Source.UNKNOWN.text;

	/**
	 * Called when logging into a world or if the config changes while in a world.
	 */
	public static void registerData()
	{
		loadItemLightLevels();
		loadEntityLightLevels();
		loadBlockTextureLightLevels();
		loadArmorTrimLightLevels();
		loadEntityLightSourcePosGetters();
	}

	@SuppressWarnings("deprecation")
	private static void loadItemLightLevels()
	{
		if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("Registering item light levels");
		ItemLightingRegistry.clear();

		ItemLightingRegistry registry = new ItemLightingRegistry();

		// Registry data
		if (LucentConfig.CLIENT.vanillaDynamicLighting.get())
		{
			LucentRegistry.setSource(Source.REGISTRY);
			Minecraft mc = Minecraft.getInstance();
			if (mc.level != null)
			{
				BuiltInRegistries.ITEM.forEach(item ->
				{
					int lightLevel = getItemLightLevel(item);
					if (lightLevel > 0)
						registry.register(item, lightLevel);
				});
			}
		}

		// Plugins
		LucentClient.getPlugins().forEach(plugin ->
		{
			LucentRegistry.setSource(Source.PLUGIN, LucentClient.pluginManager().getPluginName(plugin));
			plugin.registerItemLightings(registry);
		});

		// Resource Packs
		LucentRegistry.setSource(Source.RESOURCE_PACK);
		LucentAssets.ITEM_LIGHTING.getData().values().forEach(registry::register);

		// Config
		LucentRegistry.setSource(Source.CONFIG);
		loadRegistryConfigLightLevels(LucentConfig.CLIENT.itemBrightnessOverrides, BuiltInRegistries.ITEM, registry::register, "item", "minecraft:torch=14");

		LucentRegistry.setSource(Source.UNKNOWN);
		ItemLightingRegistry.clearCache();
	}

	public static final BlockPos DEFAULT_POS = new BlockPos(0, 70, 0);

	public static int getItemLightLevel(Item item)
	{
		int lightLevel = 0;
		try
		{
			Minecraft mc = Minecraft.getInstance();
			if (item instanceof BlockItem blockItem)
				lightLevel = blockItem.getBlock().defaultBlockState().getLightEmission(mc.level, DEFAULT_POS);
			else if (item instanceof BucketItem bucketItem)
				lightLevel = bucketItem.getFluid().defaultFluidState().createLegacyBlock().getLightEmission(mc.level, DEFAULT_POS);
		}
		catch (Exception e)
		{
			if (LucentConfig.CLIENT.logRegistry.get())
				LucentMod.LOGGER.error("Couldn't get light level for {}. {}", Optional.<Object>ofNullable(BuiltInRegistries.ITEM.getKey(item)).orElse(item), e);
		}
		return lightLevel;
	}

	@SuppressWarnings("deprecation")
	private static void loadEntityLightLevels()
	{
		if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("Registering entity light levels");
		EntityLightingRegistry.clear();

		// Plugins
		EntityLightingRegistry registry = new EntityLightingRegistry();
		LucentClient.getPlugins().forEach(plugin ->
		{
			LucentRegistry.setSource(Source.PLUGIN, LucentClient.pluginManager().getPluginName(plugin));
			plugin.registerEntityLightings(registry);
		});

		// Resource Packs
		LucentRegistry.setSource(Source.RESOURCE_PACK);
		LucentAssets.ENTITY_LIGHTING.getData().values().forEach(registry::register);

		// Configs
		LucentRegistry.setSource(Source.CONFIG);
		loadRegistryConfigLightLevels(LucentConfig.CLIENT.entityBrightnessOverrides, BuiltInRegistries.ENTITY_TYPE, registry::register, "entity", "minecraft:blaze=9");

		LucentRegistry.setSource(Source.UNKNOWN);
		EntityLightingRegistry.clearCache();
	}

	@SuppressWarnings("deprecation")
	private static void loadBlockTextureLightLevels()
	{
		if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("Registering block texture light levels");
		BlockTextureLightingRegistry.clear();

		// Plugins
		BlockTextureLightingRegistry registry = new BlockTextureLightingRegistry();
		LucentClient.getPlugins().forEach(plugin ->
		{
			LucentRegistry.setSource(Source.PLUGIN, LucentClient.pluginManager().getPluginName(plugin));
			plugin.registerBlockTextureLightings(registry);
		});

		// Resource Packs
		LucentRegistry.setSource(Source.RESOURCE_PACK);
		LucentAssets.EMISSIVE_BLOCK_TEXTURE.getData().values().forEach(registry::register);

		// Configs
		LucentRegistry.setSource(Source.CONFIG);
		loadNameConfigLightLevels(LucentConfig.CLIENT.blockTextureBrightnessOverrides, registry::register, "block texture", "minecraft:block/end_portal_frame_eye=12");

		LucentRegistry.setSource(Source.UNKNOWN);
	}

	@SuppressWarnings("deprecation")
	private static void loadArmorTrimLightLevels()
	{
		if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("Registering armor trim texture light levels");
		ArmorTrimLightingRegistry.clear();

		// Plugins
		ArmorTrimLightingRegistry registry = new ArmorTrimLightingRegistry();
		LucentClient.getPlugins().forEach(plugin ->
		{
			LucentRegistry.setSource(Source.PLUGIN, LucentClient.pluginManager().getPluginName(plugin));
			plugin.registerArmorTrimTextureLightings(registry);
		});

		// Resource Packs
		LucentRegistry.setSource(Source.RESOURCE_PACK);
		LucentAssets.EMISSIVE_ARMOR_TRIM_TEXTURE.getData().values().forEach(registry::register);

		// Configs
		LucentRegistry.setSource(Source.CONFIG);
		loadRegistryConfigLightLevels(LucentConfig.CLIENT.armorTrimBrightnessOverrides, Registries.TRIM_MATERIAL, registry::register, "armor trim texture", "minecraft:gold=10");

		LucentRegistry.setSource(Source.UNKNOWN);
	}

	private static void loadEntityLightSourcePosGetters()
	{
		EntityLightSourcePosRegistry.REGISTRY.clear();

		// Plugins
		EntityLightSourcePosRegistry registry = new EntityLightSourcePosRegistry((entity, getterFunc) -> EntityLightSourcePosRegistry.REGISTRY.put(entity, getterFunc));
		LucentClient.getPlugins().forEach(plugin ->
		{
			LucentRegistry.setSource(Source.PLUGIN, LucentClient.pluginManager().getPluginName(plugin));
			plugin.registerEntityLightSourcePositionGetter(registry);
		});

		LucentRegistry.setSource(Source.UNKNOWN);
	}

	protected static void setSource(Source source)
	{
		setSource(source, source.text);
	}

	protected static void setSource(Source source, String extra)
	{
		LucentRegistry.currentSource = source;
		LucentRegistry.sourceName = source.text + " (" + extra + ")";
	}

	private static <T> void loadRegistryConfigLightLevels(ModConfigSpec.ConfigValue<List<? extends String>> values, Registry<T> registry, BiConsumer<T, Integer> registerMethod, String name, String example)
	{
		values.get().forEach(s ->
		{
			if (s.contains("="))
			{
				String[] data = s.split("=");
				try
				{
					ResourceLocation registryObjectName = new ResourceLocation(data[0]);
					if (registry.containsKey(registryObjectName))
					{
						T registryObject = registry.get(registryObjectName);
						Integer lightLevel = Integer.parseInt(data[1]);
						registerMethod.accept(registryObject, lightLevel);
					}
				}
				catch (ResourceLocationException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The " + name + " ID may have disallowed characters. " + e.getLocalizedMessage());
				}
				catch (NumberFormatException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The brightness number could not be read as a number. " + e.getLocalizedMessage());
				}
			}
			else
				LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. It must be formatted as \"<" + name + "_id>=<brightness>\". Example, \"" + example + "\"");

		});
	}

	private static <T> void loadRegistryConfigLightLevels(ModConfigSpec.ConfigValue<List<? extends String>> values, ResourceKey<Registry<T>> registryKey, BiConsumer<ResourceKey<T>, Integer> registerMethod, String name, String example)
	{
		values.get().forEach(s ->
		{
			if (s.contains("="))
			{
				String[] data = s.split("=");
				try
				{
					ResourceLocation registryObjectName = new ResourceLocation(data[0]);
					ResourceKey<T> key = ResourceKey.create(registryKey, registryObjectName);
					Integer lightLevel = Integer.parseInt(data[1]);
					registerMethod.accept(key, lightLevel);
				}
				catch (ResourceLocationException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The " + name + " ID may have disallowed characters. " + e.getLocalizedMessage());
				}
				catch (NumberFormatException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The brightness number could not be read as a number. " + e.getLocalizedMessage());
				}
			}
			else
				LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. It must be formatted as \"<" + name + "_id>=<brightness>\". Example, \"" + example + "\"");

		});
	}

	private static void loadNameConfigLightLevels(ModConfigSpec.ConfigValue<List<? extends String>> values, BiConsumer<ResourceLocation, Integer> registerMethod, String name, String example)
	{
		values.get().forEach(s ->
		{
			if (s.contains("="))
			{
				String[] data = s.split("=");
				try
				{
					ResourceLocation registryObjectName = new ResourceLocation(data[0]);
					Integer lightLevel = Integer.parseInt(data[1]);
					registerMethod.accept(registryObjectName, lightLevel);
				}
				catch (ResourceLocationException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The " + name + " may have disallowed characters. " + e.getLocalizedMessage());
				}
				catch (NumberFormatException e)
				{
					LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. The brightness number could not be read as a number. " + e.getLocalizedMessage());
				}
			}
			else
				LucentMod.LOGGER.error("\"" + s + "\" is not a valid config value. It must be formatted as \"<" + name + ">=<brightness>\". Example, \"" + example + "\"");

		});
	}

	public static int clampLight(int lightLevel)
	{
		return Mth.clamp(lightLevel, 0, 15);
	}

	public static void logInvalidResourceLocation(String attemptedResourceLocation)
	{
		LucentMod.LOGGER.warn("{} is not a valid resource location.", attemptedResourceLocation);
	}

	public static enum Source
	{
		UNKNOWN("Unknown", 0, true),
		REGISTRY("Registry", 1, true),
		PLUGIN("Plugin", 2, false),
		RESOURCE_PACK("Resource Pack", 3, false),
		CONFIG("Config", 4, false);

		private final String text;
		public final int priority;
		public final boolean isBuiltin;

		Source(String text, int priority, boolean isBuiltin)
		{
			this.text = text;
			this.priority = priority;
			this.isBuiltin = isBuiltin;
		}

		@Override
		public String toString()
		{
			return this.text;
		}
	}
}
