package com.legacy.lucent.core.block_emission;

import com.legacy.lucent.api.registry.BlockTextureLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Handles getting the light level of the specified block texture for emissive
 * rendering.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class BlockEmissionEngine
{
	/**
	 * Gets the light level of the passed texture
	 * 
	 * @param original
	 *            The original packed light level at the location
	 * @param spriteName
	 *            The name of the texture file
	 * @return The new calculated light level, or the original if it was brighter
	 */
	public static int calcLight(int original, ResourceLocation spriteName)
	{
		if (LucentConfig.CLIENT.blockTexturesGlow.get())
		{
			int blockLight = BlockTextureLightingRegistry.get(spriteName);
			if (LightTexture.block(original) < blockLight)
				return LightTexture.pack(blockLight, LightTexture.sky(original));
		}
		return original;
	}

	/**
	 * Gets the light level of the passed quad from its sprite
	 * 
	 * @param original
	 *            The original light level at the location
	 * @param quad
	 *            The quad to check
	 * @return The new calculated light level, or the original if it was brighter
	 */
	public static int calcLight(int original, BakedQuad quad)
	{
		return calcLight(original, quad.getSprite().contents().name());
	}
}
