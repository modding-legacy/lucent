package com.legacy.lucent.core.plugin;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

@LucentPlugin
@OnlyIn(Dist.CLIENT)
public final class BlueSkiesPlugin implements ILucentPlugin
{
	@Override
	public void registerItemLightings(ItemLightingRegistry registry)
	{
		registry.register(this.locate("moonstone_shard"), 7);
		registry.register(this.locate("moonstone_shield"), 7);
	}

	@Override
	public void registerEntityLightings(EntityLightingRegistry registry)
	{
		registry.register(this.locate("emberback"), 7);
		registry.register(this.locate("firefly"), 5);
		registry.register(this.locate("frost_spirit"), 5);
	}

	@Override
	public String ownerModID()
	{
		return "blue_skies";
	}

	@Override
	public boolean isInternal()
	{
		return true;
	}
	
	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.blueSkiesPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}
}
