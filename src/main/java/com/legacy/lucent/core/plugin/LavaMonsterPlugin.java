package com.legacy.lucent.core.plugin;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.minecraft.world.level.block.Blocks;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

@LucentPlugin
@OnlyIn(Dist.CLIENT)
public final class LavaMonsterPlugin implements ILucentPlugin
{
	@SuppressWarnings("deprecation")
	@Override
	public void registerEntityLightings(EntityLightingRegistry registry)
	{
		registry.register(this.locate("lava_monster"), Blocks.LAVA.defaultBlockState().getLightEmission());
	}

	@Override
	public String ownerModID()
	{
		return "lava_monster";
	}
	
	@Override
	public boolean isInternal()
	{
		return true;
	}
	
	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.lavaMonsterPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}
}
