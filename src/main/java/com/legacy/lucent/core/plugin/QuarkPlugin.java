package com.legacy.lucent.core.plugin;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.minecraft.nbt.CompoundTag;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

@LucentPlugin
@OnlyIn(Dist.CLIENT)
public final class QuarkPlugin implements ILucentPlugin
{
	@Override
	public void registerEntityLightings(EntityLightingRegistry registry)
	{
		registry.register(this.locate("foxhound"), entity ->
		{
			return entity.saveWithoutId(new CompoundTag()).getBoolean("IsBlue") ? 7 : 10;
		});
	}

	@Override
	public String ownerModID()
	{
		return "quark";
	}

	@Override
	public boolean isInternal()
	{
		return true;
	}
	
	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.quarkPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}
}
