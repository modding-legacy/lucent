package com.legacy.lucent.core.plugin;

import java.util.List;
import java.util.Optional;

import com.legacy.lucent.api.data.objects.LightLevelProvider;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.common.ModConfigSpec;

public class TinkersConstructPlugin implements ILucentPlugin
{
	@Override
	public void registerItemLightings(ItemLightingRegistry registry)
	{
		String ownerMod = this.ownerModID();
		List<Item> ticItems = registry.getMatchingByName(rl -> rl.getNamespace().equals(ownerMod));

		// Tinkers tools with glowing
		for (var item : ticItems)
			registry.register(item, new LightLevelProvider.IfNbt<>("{tic_modifiers:[{name:'tconstruct:glowing'}]}", 14));

		// Tinkers lanterns glow based on fluids
		var lanternProv = new LightLevelProvider.MatchNbtFluid<ItemStack>("tank.FluidName");
		Optional.ofNullable(BuiltInRegistries.ITEM.get(locate("seared_lantern"))).ifPresent(item -> registry.register(item, lanternProv));
		Optional.ofNullable(BuiltInRegistries.ITEM.get(locate("scorched_lantern"))).ifPresent(item -> registry.register(item, lanternProv));
	}

	@Override
	public String ownerModID()
	{
		return "tconstruct";
	}

	@Override
	public boolean isInternal()
	{
		return true;
	}
	
	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.tinkersConstructPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}
}
