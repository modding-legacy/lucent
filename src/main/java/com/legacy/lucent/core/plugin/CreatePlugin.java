package com.legacy.lucent.core.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.legacy.lucent.api.EntityBrightnessMap;
import com.legacy.lucent.api.VecTransformer;
import com.legacy.lucent.api.data.objects.ItemLighting;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.compat.create.IContraptionEntityInfo;
import com.legacy.lucent.core.reflection.ReflectedClass;
import com.legacy.lucent.core.reflection.ReflectedField;
import com.mojang.datafixers.util.Pair;

import net.minecraft.ReportedException;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

@LucentPlugin
@OnlyIn(Dist.CLIENT)
public class CreatePlugin implements ILucentPlugin
{
	/**
	 * Contraption Trains and other free-moving things
	 */
	public static final ReflectedClass ORIENTED_CONTRAPTION = ReflectedClass.of("com.simibubi.create.content.contraptions.OrientedContraptionEntity");
	/**
	 * Stationary Contraption Windmills, pistons, bearings
	 */
	public static final ReflectedClass CONTROLLED_CONTRAPTION = ReflectedClass.of("com.simibubi.create.content.contraptions.ControlledContraptionEntity");

	public static final ReflectedField<Float> ORIENTED_CONTRAPTION_YAW = ReflectedField.of(ORIENTED_CONTRAPTION, "yaw", Float.class, () -> 0.0F);
	public static final ReflectedField<Float> ORIENTED_CONTRAPTION_PITCH = ReflectedField.of(ORIENTED_CONTRAPTION, "pitch", Float.class, () -> 0.0F);

	public static final ReflectedField<Direction.Axis> CONTROLLED_CONTRAPTION_AXIS = ReflectedField.of(CONTROLLED_CONTRAPTION, "rotationAxis", Direction.Axis.class, () -> Direction.Axis.Y);
	public static final ReflectedField<Float> CONTROLLED_CONTRAPTION_ANGLE = ReflectedField.of(CONTROLLED_CONTRAPTION, "angle", Float.class, () -> 0.0F);

	@Override
	public void registerItemLightings(ItemLightingRegistry registry)
	{
		if (!LucentConfig.CLIENT.createPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false))
			return;

		registry.register(ItemLighting.builder().item(this.locate("blaze_burner")).light(7)); // Value of Blaze
	}

	@Override
	public void getAdditionalEntityLightLevels(EntityBrightnessMap map)
	{
		if (!LucentConfig.CLIENT.createPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false))
			return;

		try
		{
			Entity entity = map.getEntity();
			storePackedData(entity);
			addLighting(map, entity);
		}
		catch (ReportedException e)
		{
			// If we try to get entity data early this could get angry
		}
		catch (Throwable t)
		{
			LucentMod.LOGGER.error(t);
			LucentMod.LOGGER.printStacktrace("Encountered an error with Create. You can disable Create compat in the config to prevent further errors.", t);
		}
	}

	private static void storePackedData(Entity entity)
	{
		if (entity instanceof IContraptionEntityInfo contraption && contraption.lucent$shouldAttemptRead())
		{
			contraption.lucent$incrementReadAttempt();
			Data data = readStaticData(entity.saveWithoutId(new CompoundTag()));
			if (data != Data.INVALID)
			{
				contraption.lucent$setLightData(data);
			}
		}
	}

	private static void addLighting(EntityBrightnessMap map, Entity entity)
	{
		if (entity instanceof IContraptionEntityInfo contraption)
		{
			Data data = contraption.lucent$getLightData();
			if (data != null)
			{
				for (var pair : data.lights)
				{
					VecTransformer pos = getEmissionPos(pair.getFirst(), contraption);
					if (pos != null)
						map.add(pos, pair.getSecond());
				}
			}
		}
	}

	public static VecTransformer getEmissionPos(BlockPos pos, IContraptionEntityInfo contraption)
	{
		Vec3 posVec = new Vec3(pos.getX(), pos.getY(), pos.getZ());
		if (CreatePlugin.CONTROLLED_CONTRAPTION.isInstance(contraption)) // Stationary Contraption
		{
			Axis axis = CreatePlugin.CONTROLLED_CONTRAPTION_AXIS.get(contraption);
			return new VecTransformer.Rotated(posVec, CreatePlugin.CONTROLLED_CONTRAPTION_ANGLE.get(contraption), axis == null ? Axis.Y : axis);
		}
		if (CreatePlugin.ORIENTED_CONTRAPTION.isInstance(contraption) && !contraption.lucent$getLightData().type.equals("stabilized")) // Contraption
		{
			return new VecTransformer.PitchAndYaw(VecTransformer.rotateVec(posVec, contraption.lucent$getLightData().initOrientation().toYRot(), Axis.Y), CreatePlugin.ORIENTED_CONTRAPTION_PITCH.get(contraption), -CreatePlugin.ORIENTED_CONTRAPTION_YAW.get(contraption), Axis.Z);
		}
		return new VecTransformer.Relative(posVec);
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	private static Data readStaticData(CompoundTag tag)
	{
		List<String> missingTags = new ArrayList<>();

		if (contains(tag, "Contraption", Tag.TAG_COMPOUND, missingTags))
		{
			CompoundTag contraption = tag.getCompound("Contraption");
			if (contains(contraption, "Blocks", Tag.TAG_COMPOUND, missingTags))
			{
				CompoundTag blocks = contraption.getCompound("Blocks");
				if (contains(blocks, "Palette", Tag.TAG_LIST, missingTags) && contains(blocks, "BlockList", Tag.TAG_LIST, missingTags))
				{
					// Read states for palette
					ListTag palette = blocks.getList("Palette", Tag.TAG_COMPOUND);
					Map<Integer, BlockState> states = new HashMap<>();
					for (int i = 0; i < palette.size(); i++)
					{
						if (palette.get(i) instanceof CompoundTag c)
						{
							if (contains(c, "Name", Tag.TAG_STRING, missingTags))
							{
								ResourceLocation name = new ResourceLocation(c.getString("Name"));
								Block block = BuiltInRegistries.BLOCK.get(name);
								if (block != null)
								{
									BlockState state = block.defaultBlockState();
									Map<String, Property<?>> propertiesMap = state.getProperties().stream().collect(Collectors.toMap(p -> p.getName(), p -> p));

									if (contains(c, "Properties", Tag.TAG_COMPOUND, missingTags))
									{
										CompoundTag properties = c.getCompound("Properties");
										for (String k : properties.getAllKeys())
										{
											Property<?> prop = propertiesMap.get(k);
											if (prop != null && state.hasProperty(prop))
											{
												String v = properties.getString(k);
												Optional<?> opVal = prop.getValue(v);
												if (opVal.isPresent())
												{
													state = state.setValue((Property) prop, (Comparable) opVal.get());
												}
											}
										}
									}
									states.put(i, state);
								}
							}
						}
					}

					// Read blocks using palette
					ListTag blockList = blocks.getList("BlockList", Tag.TAG_COMPOUND);
					List<Pair<BlockPos, BlockState>> statesByPos = new ArrayList<>();
					for (Tag blockInfo : blockList)
					{
						if (blockInfo instanceof CompoundTag c)
						{
							if (contains(c, "Pos", Tag.TAG_LONG, missingTags) && contains(c, "State", Tag.TAG_INT, missingTags))
							{
								int stateID = c.getInt("State");
								BlockState state = states.get(stateID);
								if (state != null)
								{
									BlockPos pos = BlockPos.of(c.getLong("Pos"));
									statesByPos.add(Pair.of(pos, state));
								}
							}
						}
					}

					String type = "";
					if (contains(contraption, "Type", Tag.TAG_STRING, missingTags))
					{
						type = contraption.getString("Type");
					}

					Direction initialOrientation = null;
					if (contains(tag, "InitialOrientation", Tag.TAG_STRING, missingTags))
					{
						initialOrientation = Direction.byName(tag.getString("InitialOrientation").toLowerCase(Locale.ENGLISH));
					}
					if (initialOrientation == null)
						initialOrientation = Direction.NORTH;

					List<Pair<BlockPos, Integer>> lights = new ArrayList<>();
					for (var stateAndPos : statesByPos)
					{
						int l = stateAndPos.getSecond().getLightEmission();
						if (l > 0)
							lights.add(Pair.of(stateAndPos.getFirst(), l));
					}
					return new Data(lights, type, initialOrientation);
				}
			}
		}

		// We should only get here if we failed to read the data
		if (!missingTags.isEmpty())
		{
			LucentMod.LOGGER.error("Could not read nbt tags from a Create contraption. They may have moved or are missing.", String.join(", ", missingTags));
		}
		return Data.INVALID;
	}

	private static boolean contains(CompoundTag tag, String key, byte type, List<String> missing)
	{
		if (tag.contains(key, type))
			return true;
		missing.add(key + "=" + missing);
		return false;
	}

	@Override
	public String ownerModID()
	{
		return "create";
	}

	@Override
	public boolean isInternal()
	{
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.createPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}

	public static record Data(List<Pair<BlockPos, Integer>> lights, String type, Direction initOrientation)
	{
		private static final Data INVALID = new Data(Collections.emptyList(), "", Direction.NORTH);
	}
}
