package com.legacy.lucent.core.plugin;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.core.LucentConfig;

import net.minecraft.world.entity.monster.Slime;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.ModConfigSpec;

@LucentPlugin
@OnlyIn(Dist.CLIENT)
public final class NethercraftPlugin implements ILucentPlugin
{
	@Override
	public void registerEntityLightings(EntityLightingRegistry registry)
	{
		registry.register(this.locate("lava_slime"), entity ->
		{
			if (entity instanceof Slime lavaSlime)
				return (int) ((lavaSlime.getSize() + 1) * 1.5D);
			return 5;
		});

		registry.register(this.locate("ghast_bomb"), 9);
	}

	@Override
	public String ownerModID()
	{
		return "nethercraft";
	}

	@Override
	public boolean isInternal()
	{
		return true;
	}
	
	@Override
	public boolean isEnabled()
	{
		return LucentConfig.CLIENT.nethercraftPlugin.map(ModConfigSpec.BooleanValue::get).orElse(false);
	}
}
