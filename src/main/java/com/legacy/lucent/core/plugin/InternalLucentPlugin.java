package com.legacy.lucent.core.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.legacy.lucent.api.EntityBrightness;
import com.legacy.lucent.api.EntityBrightnessMap;
import com.legacy.lucent.api.data.objects.EmissiveArmorTrimTexture;
import com.legacy.lucent.api.data.objects.EntityLighting;
import com.legacy.lucent.api.data.objects.ItemLighting;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.plugin.LucentPlugin;
import com.legacy.lucent.api.registry.ArmorTrimLightingRegistry;
import com.legacy.lucent.api.registry.BlockTextureLightingRegistry;
import com.legacy.lucent.api.registry.EntityLightSourcePosRegistry;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.LucentClient;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Display.BlockDisplay;
import net.minecraft.world.entity.Display.ItemDisplay;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.GlowSquid;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.decoration.ItemFrame;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.MagmaCube;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.armortrim.TrimMaterials;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CampfireBlock;
import net.minecraft.world.level.block.CarvedPumpkinBlock;
import net.minecraft.world.level.block.FireBlock;
import net.minecraft.world.level.block.LightBlock;
import net.minecraft.world.level.block.TorchBlock;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * The default implementation of ILucentPlugin. Reference this for how to add
 * your own. This plugin should always be processed first.
 * 
 * @author Silver_David
 *
 */
@LucentPlugin
@OnlyIn(Dist.CLIENT)
public final class InternalLucentPlugin implements ILucentPlugin
{
	@SuppressWarnings("deprecation")
	@Override
	public void registerItemLightings(final ItemLightingRegistry registry)
	{
		if (LucentConfig.CLIENT.vanillaDynamicLighting.get())
		{
			registry.register(Items.BLAZE_ROD, 11);
			registry.register(Items.NETHER_STAR, 15);
			registry.register(Items.PRISMARINE_CRYSTALS, 9);
			registry.register(Items.SPECTRAL_ARROW, 5);
			registry.register(Items.GLOWSTONE_DUST, 10);
			registry.register(Items.GLOW_BERRIES, 9);
			registry.register(Items.GLOW_INK_SAC, 5);
			registry.register(Items.GLOW_ITEM_FRAME, 2);
			registry.register(Items.GLOW_LICHEN, 7);
			registry.register(ItemLighting.builder().item(Items.LIGHT).light(stack ->
			{
				CompoundTag stateTag = stack.getTagElement("BlockStateTag");
				try
				{
					if (stateTag != null)
					{
						Tag tag = stateTag.get(LightBlock.LEVEL.getName());
						if (tag != null)
							return Integer.parseInt(tag.getAsString());
					}
				}
				catch (NumberFormatException numberformatexception)
				{}
				return 15;
			}));

			// Remove things that shouldn't work underwater
			if (LucentConfig.CLIENT.torchesDoNothingInWater.get())
			{
				for (Item i : BuiltInRegistries.ITEM)
				{
					if (i instanceof BlockItem)
					{
						Block b = ((BlockItem) i).getBlock();
						int l = b.defaultBlockState().getLightEmission();
						if (l > 0 && (b instanceof TorchBlock || b instanceof CampfireBlock || b instanceof CarvedPumpkinBlock || b instanceof FireBlock))
						{
							registry.register(ItemLighting.builder().item(i).light(l).worksUnderwater(false));
						}
					}
				}
			}
		}
	}

	@Override
	public void registerEntityLightings(final EntityLightingRegistry registry)
	{
		if (LucentConfig.CLIENT.vanillaDynamicLighting.get())
		{
			// Glow item frame intentionally left out
			registry.register(EntityType.BLAZE, 9);
			registry.register(EntityType.SPECTRAL_ARROW, 5);
			registry.register(EntityType.DRAGON_FIREBALL, 11);

			registry.register(EntityLighting.builder().entity(EntityType.ITEM).light((ItemEntity itemEntity) -> LucentConfig.CLIENT.itemEntitiesGlow.get() ? ItemLightingRegistry.get(itemEntity.getItem()) : 0));
			registry.register(EntityLighting.builder().entity(EntityType.MAGMA_CUBE).light((MagmaCube magmaCube) -> (magmaCube.getSize() + 1) * 2));
			registry.register(EntityLighting.builder().entity(EntityType.GLOW_SQUID).light((GlowSquid glowSquid) -> (int) Mth.clampedLerp(0.0F, 7.0F, 1.0F - glowSquid.getDarkTicksRemaining() / 10.0F)));
			registry.register(EntityLighting.builder().entity(EntityType.CREEPER).light((Creeper creeper) -> LucentConfig.CLIENT.creepersGlow.get() ? (int) (10 * creeper.getSwelling(0.0F)) : 0));
			registry.register(EntityLighting.builder().entity(EntityType.FALLING_BLOCK).light((FallingBlockEntity fallingBlock) -> fallingBlock.getBlockState().getLightEmission()));
			registry.register(EntityLighting.builder().entity(EntityType.BLOCK_DISPLAY).light((BlockDisplay display) -> display.blockRenderState().blockState().getLightEmission()));
			registry.register(EntityLighting.builder().entity(EntityType.ITEM_DISPLAY).light((ItemDisplay display) -> ItemLightingRegistry.get(display.itemRenderState().itemStack())));
		}
	}

	@Override
	public void registerBlockTextureLightings(final BlockTextureLightingRegistry registery)
	{
		int soulLight = 14;

		registery.register("block/end_portal_frame_eye", 12);
		registery.register("block/soul_lantern", soulLight);
		registery.register("block/soul_campfire_fire", soulLight);
		registery.register("block/soul_fire_0", soulLight);
		registery.register("block/soul_fire_1", soulLight);
		registery.register("block/soul_torch", soulLight);
	}

	@Override
	public void registerEntityLightSourcePositionGetter(final EntityLightSourcePosRegistry registry)
	{
		registry.register(EntityType.ARROW, Entity::position);
		registry.register(EntityType.SPECTRAL_ARROW, Entity::position);
	}

	@Override
	public void getEntityLightLevel(final EntityBrightness entityBrightness)
	{
		Entity entity = entityBrightness.getEntity();

		if (entity.isSpectator())
			return;

		// Registered light levels
		entityBrightness.setLightLevel(EntityLightingRegistry.get(entity));

		// Burning, glowing, etc
		if (LucentConfig.CLIENT.vanillaDynamicLighting.get())
		{
			if (LucentConfig.CLIENT.burningEntitiesGlow.get() && entity.isOnFire())
				entityBrightness.setLightLevel(LucentConfig.CLIENT.burningEntityLightLevel.get());

			if (LucentConfig.CLIENT.glowingEntitiesGlow.get() && entity.isCurrentlyGlowing())
				entityBrightness.setLightLevel(LucentConfig.CLIENT.glowingEntityLightLevel.get());
		}

		// Equipped Items
		if (LucentConfig.CLIENT.heldItemsGlow.get())
		{
			List<ItemStack> equipped = new ArrayList<>(6);
			LucentClient.pluginManager().forEach(plugin -> equipped.addAll(plugin.gatherEquippedItems(entity)));
			int light = 0;
			for (ItemStack stack : equipped)
				light = Math.max(light, ItemLightingRegistry.get(stack));
			if (light > 0)
				entityBrightness.setLightLevel(light);
		}
	}

	@Override
	public List<ItemStack> gatherEquippedItems(Entity entity)
	{
		if (entity instanceof LivingEntity livingEntity)
		{
			List<ItemStack> items = new ArrayList<>(6);
			for (EquipmentSlot slot : EquipmentSlot.values())
				items.add(livingEntity.getItemBySlot(slot));
			return items;
		}
		else if (entity instanceof ItemFrame itemFrame && LucentConfig.CLIENT.itemFramesGlow.get())
		{
			return List.of(itemFrame.getItem());
		}
		return Collections.emptyList();
	}

	@Override
	public void getAdditionalEntityLightLevels(EntityBrightnessMap map)
	{
		// General testing
		/*Entity e = map.getEntity();
		if (e.is(Minecraft.getInstance().player))
		{
			map.add(new VecTransformer.RaytracedFacing(64, new Vec3(0, e.getEyeHeight(), 0)), 10);
		}*/
	}

	@Override
	public void registerArmorTrimTextureLightings(ArmorTrimLightingRegistry registry)
	{
		registry.register(new EmissiveArmorTrimTexture(TrimMaterials.AMETHYST, 5));
	}

	@Override
	public String ownerModID()
	{
		return LucentMod.MODID;
	}

	@Override
	public int getPriority()
	{
		return Integer.MAX_VALUE;
	}
}
