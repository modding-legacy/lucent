package com.legacy.lucent.core.reflection;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.lucent.core.LucentMod;

import net.neoforged.fml.util.ObfuscationReflectionHelper;
import net.neoforged.neoforge.common.util.Lazy;

public record ReflectedField<T> (Lazy<Optional<Field>> field, String fieldName, Class<T> fieldClass, Supplier<T> defaultRet)
{
	public static <T> ReflectedField<T> of(Supplier<Optional<Class<?>>> ownerClass, String fieldName, Class<T> fieldClass, Supplier<T> defaultRet)
	{
		return new ReflectedField<>(Lazy.of(() ->
		{
			Optional<Class<?>> opClass = ownerClass.get();
			if (opClass.isPresent())
			{
				try
				{
					return Optional.ofNullable(ObfuscationReflectionHelper.findField(opClass.get(), fieldName));
				}
				catch (Throwable e)
				{
					LucentMod.LOGGER.printStacktrace("Failed to reflect a field. Class: " + opClass.get() + ", Field: " + fieldName, e);
				}
			}
			else
			{
				LucentMod.LOGGER.error("Couldn't obtain reflected class for field: " + fieldName);
			}
			return Optional.empty();
		}), fieldName, fieldClass, defaultRet);
	}

	@SuppressWarnings("unchecked")
	public T get(@Nullable Object obj)
	{
		try
		{
			Optional<Field> f = this.field.get();
			if (f.isPresent())
			{
				Object ret = f.get().get(obj);
				if (this.fieldClass.isInstance(ret))
					return (T) ret;
			}
		}
		catch (Throwable t)
		{
			LucentMod.LOGGER.printStacktrace("Failed to get a reflected value for " + this.fieldClass.getName() + "." + this.fieldName, t);
		}
		return this.defaultRet.get();
	}
}