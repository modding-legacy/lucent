package com.legacy.lucent.core.reflection;

import java.util.Optional;
import java.util.function.Supplier;

import com.legacy.lucent.core.LucentMod;

import net.neoforged.neoforge.common.util.Lazy;

public record ReflectedClass(Lazy<Optional<Class<?>>> clazz) implements Supplier<Optional<Class<?>>>
{
	public static ReflectedClass of(String classPath)
	{
		return new ReflectedClass(() ->
		{
			try
			{
				return Optional.ofNullable(Class.forName(classPath));
			}
			catch (ClassNotFoundException e)
			{
				LucentMod.LOGGER.printStacktrace("Failed to reflect a class. Class: " + classPath, e);
				return Optional.empty();
			}
		});
	}
	
	@Override
	public Optional<Class<?>> get()
	{
		return this.clazz.get();
	}
	
	public boolean isInstance(Object obj)
	{
		var opClass = this.get();
		return opClass.isPresent() && opClass.get().isInstance(obj);
	}
}
