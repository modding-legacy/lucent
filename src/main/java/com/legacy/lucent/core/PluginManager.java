package com.legacy.lucent.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Logger;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModList;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.neoforgespi.language.ModFileScanData;
import net.neoforged.neoforgespi.language.ModFileScanData.AnnotationData;

/**
 * An automatic plugin handler for mods with plugin based APIs. Create a static
 * instance of this at sometime before the {@link FMLCommonSetupEvent}
 * 
 * @author Silver_David
 *
 * @param <P>
 */
public class PluginManager<P extends PluginManager.IBasePlugin> implements Iterable<P>
{
	private List<P> plugins = Collections.emptyList();
	private final Class<P> pluginClass;
	private final Class<?> annotationClass;
	private final Supplier<Logger> loggerGetter;
	private final Supplier<String> modIDGetter;

	public PluginManager(Class<P> pluginClass, Class<?> annotationClass, Supplier<Logger> logger, Supplier<String> modID, IEventBus modBus)
	{
		this.pluginClass = pluginClass;
		this.annotationClass = annotationClass;
		this.loggerGetter = logger;
		this.modIDGetter = modID;

		try
		{
			// Prevents a crash if the game fails to load due to another mod, keeping us out of the crashlog when we shouldn't be there
			modBus.addListener(EventPriority.HIGHEST, this::loadPlugins);
		}
		catch (Exception e)
		{}
	}

	public List<P> get()
	{
		return this.plugins;
	}

	/**
	 * Runs the predicate for each plugin.
	 * 
	 * @return True if the operation should be canceled
	 */
	public boolean post(Predicate<P> predicate)
	{
		return this.plugins.stream().anyMatch(predicate);
	}

	@Override
	public Iterator<P> iterator()
	{
		return this.plugins.iterator();
	}

	@SuppressWarnings("unchecked")
	public void loadPlugins(FMLCommonSetupEvent event)
	{
		Logger logger = this.loggerGetter.get();
		String modID = this.modIDGetter.get();

		// Locate plugin classes
		org.objectweb.asm.Type modPlugin = org.objectweb.asm.Type.getType(this.annotationClass);
		List<String> pluginClassNames = ModList.get().getAllScanData().stream().map(ModFileScanData::getAnnotations).flatMap(Collection::stream).filter(a -> modPlugin.equals(a.annotationType())).map(AnnotationData::memberName).collect(Collectors.toList());

		// Create plugin instances and store
		List<P> plugins = new ArrayList<>();
		for (String className : pluginClassNames)
		{
			try
			{
				Class<?> clazz = Class.forName(className);
				for (Class<?> i : clazz.getInterfaces())
				{
					if (i.equals(this.pluginClass))
					{
						try
						{
							P pluginInstance = (P) clazz.getDeclaredConstructor().newInstance();
							if (ModList.get().isLoaded(pluginInstance.ownerModID()))
							{
								List<String> requiredMods = pluginInstance.requiredMods();
								if ((requiredMods.isEmpty() || requiredMods.stream().allMatch(ModList.get()::isLoaded)) && pluginInstance.isEnabled())
								{
									plugins.add(pluginInstance);
									logger.info("[{}] Found and created plugin: {}", modID, className);
								}
							}
						}
						catch (Exception e)
						{
							logger.error("[{}] Failed to find or create plugin: {} {}", modID, className, e);
						}
					}
				}
			}
			catch (ClassNotFoundException e)
			{
				logger.error("[{}] Failed to locate class plugin: {} {}", modID, className, e);
			}
		}

		plugins.sort(this::compare);

		this.plugins = List.copyOf(plugins);
	}

	protected int compare(P pluginA, P pluginB)
	{
		int a = pluginA.getPriority();
		int b = pluginB.getPriority();
		return a == b ? 0 : a > b ? -1 : 1;
	}

	public String getPluginName(IBasePlugin plugin)
	{
		String s = plugin.getClass().getSimpleName();
		if (plugin.isInternal())
			s = s + " - Internal";
		return s;
	}

	/**
	 * The base interface that all plugins must extend. Contains data for priority,
	 * mod requirements, and basic helper methods.
	 * 
	 * @author Silver_David
	 *
	 */
	public static interface IBasePlugin
	{
		/**
		 * Gets the mod that owns this plugin instance. This mod is required along with
		 * {@link #requiredMods()}
		 * 
		 * @return The name of the mod that owns this plugin instance
		 */
		String ownerModID();

		/**
		 * Gets the IDs of mods that this plugin requires in order to be registered.
		 * These mods will be required along with {@link #ownerModID()}
		 * 
		 * @return The required mods for this plugin to be used
		 */
		@Nullable
		default List<String> requiredMods()
		{
			return Collections.emptyList();
		}

		/**
		 * Gets the priority of this plugin instance. Plugins with higher priority
		 * values will run first
		 * <p>
		 * By default, plugins have a priority of 0. Plugins marked as internal have a
		 * priority of 1
		 * 
		 * @return The priority of this plugin
		 */
		default int getPriority()
		{
			return this.isInternal() ? 1 : 0;
		}

		/**
		 * Gets resource location from this plugin's required mod
		 * 
		 * @param path
		 * @return
		 * @throws NullPointerException
		 *             if {@link #requiredMods()} returns null
		 */
		default ResourceLocation locate(String path) throws NullPointerException
		{
			String ownerID = this.ownerModID();
			if (ownerID == null)
				throw new NullPointerException(String.format("The owner mod ID is null for %s", this.getClass().getCanonicalName()));
			return new ResourceLocation(ownerID, path);
		}

		/**
		 * @return True if the plugin should be used.
		 */
		default boolean isEnabled()
		{
			return true;
		}

		/**
		 * Defines a plugin as built-in to the mod that owns this plugin system. You
		 * shouldn't need to implement this method on your mods.
		 * 
		 * @return
		 */
		@Deprecated
		default boolean isInternal()
		{
			return false;
		}
	}
}
