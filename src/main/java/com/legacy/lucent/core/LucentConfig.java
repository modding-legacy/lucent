package com.legacy.lucent.core;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.lucent.core.config.ConfigBuilder;
import com.legacy.lucent.core.config.ConfigValueWrapper;

import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.ModList;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.neoforge.common.ModConfigSpec;

@OnlyIn(Dist.CLIENT)
public class LucentConfig
{
	public static final ModConfigSpec CLIENT_SPEC;
	public static final ClientConfig CLIENT;

	static
	{
		{
			final Pair<ClientConfig, ModConfigSpec> clientPair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = clientPair.getLeft();
			CLIENT_SPEC = clientPair.getRight();
		}
	}

	public static class ClientConfig
	{
		// Dynamic lighting
		public final ModConfigSpec.BooleanValue vanillaDynamicLighting;
		public final ModConfigSpec.BooleanValue torchesDoNothingInWater;
		public final ConfigValueWrapper<Double, Float> deepDarkScaling;
		public final ConfigValueWrapper<Integer, Integer> lightRefreshRate;
		public final ModConfigSpec.IntValue maxVisibleDistance;
		public final ModConfigSpec.BooleanValue seeThroughWalls;
		public final ModConfigSpec.BooleanValue smoothBlending;
		public final ModConfigSpec.BooleanValue threadedRendering;

		public final ModConfigSpec.ConfigValue<List<? extends String>> entityBrightnessOverrides;

		public final ModConfigSpec.BooleanValue itemEntitiesGlow;
		public final ModConfigSpec.BooleanValue heldItemsGlow;
		public final ModConfigSpec.BooleanValue itemFramesGlow;
		public final ModConfigSpec.ConfigValue<List<? extends String>> itemBrightnessOverrides;

		public final ModConfigSpec.BooleanValue burningEntitiesGlow;
		public final ModConfigSpec.IntValue burningEntityLightLevel;

		public final ModConfigSpec.BooleanValue glowingEntitiesGlow;
		public final ModConfigSpec.IntValue glowingEntityLightLevel;

		public final ModConfigSpec.BooleanValue creepersGlow;

		// Emissive textures
		public final ModConfigSpec.BooleanValue blockTexturesGlow;
		public final ModConfigSpec.ConfigValue<List<? extends String>> blockTextureBrightnessOverrides;

		public final ModConfigSpec.BooleanValue armorTrimsGlow;
		public final ModConfigSpec.ConfigValue<List<? extends String>> armorTrimBrightnessOverrides;

		// Debug
		public final ModConfigSpec.BooleanValue logRegistry;
		public final ModConfigSpec.BooleanValue logLightCalculationTime;
		public final ModConfigSpec.BooleanValue logDeprecatedFolderStructure;
		public final ModConfigSpec.BooleanValue logDynamicLightingErrors;

		// Flags
		public final ModConfigSpec.BooleanValue disableBlockTextureGlowIfError;
		public final ModConfigSpec.BooleanValue warnAboutFlywheel;
		public final ModConfigSpec.BooleanValue warnAboutSodium;

		// Plugins
		public final Optional<ModConfigSpec.BooleanValue> blueSkiesPlugin;
		public final Optional<ModConfigSpec.BooleanValue> createPlugin;
		public final Optional<ModConfigSpec.BooleanValue> lavaMonsterPlugin;
		public final Optional<ModConfigSpec.BooleanValue> nethercraftPlugin;
		public final Optional<ModConfigSpec.BooleanValue> quarkPlugin;
		public final Optional<ModConfigSpec.BooleanValue> tinkersConstructPlugin;

		public ClientConfig(ModConfigSpec.Builder builder)
		{
			var bus = LucentMod.EVENT_BUS.get();
			bus.addListener((ModConfigEvent.Loading event) -> this.reload());
			bus.addListener((ModConfigEvent.Reloading event) -> this.reload());

			// Dynamic lighting
			builder.push("dynamic_lighting_settings");
			this.vanillaDynamicLighting = ConfigBuilder.makeBoolean(builder, "vanillaDynamicLighting", "If enabled, vanilla mobs and items will recieve dynamic lighting. If disabled, only objects specified by external mods or configs will recieve dynamic lighting.", true);
			this.torchesDoNothingInWater = ConfigBuilder.makeBoolean(builder, "torchesDoNothingInWater", "If enabled, torches and other vanilla fire-based items will not emit light underwater.", false);
			this.deepDarkScaling = ConfigValueWrapper.create(ConfigBuilder.makeDouble(builder, "deepDarkScaling", "A multiplier applied to an entity's light level when in the deep dark.", 0.5, 0.0, 1.0), Double::floatValue, bus, LucentMod.MODID);
			this.lightRefreshRate = ConfigValueWrapper.create(ConfigBuilder.makeInt(builder, "lightRefreshRate", "The maximum amount of times light tries to refresh in a second. Higher values attempt to refresh more often. This number can be decreased if you experience poor performance.", 40, 1, 240), i -> 1000 / i, bus, LucentMod.MODID);
			this.maxVisibleDistance = ConfigBuilder.makeInt(builder, "maxVisibleDistance", "Determines how far away you should be able to see light from an entity (measured in blocks). 0 for infinite distance. Use this to prevent far away entities from causing light updates.", 128, 0, 1024);
			this.seeThroughWalls = ConfigBuilder.makeBoolean(builder, "seeThroughWalls", "Determines if dynamic lighting should still occur when an entity is behind a wall. Turning this on may impact performance, but allows light to be seen around corners from far away. Entities within 24 blocks will always emit light.", false);
			this.smoothBlending = ConfigBuilder.makeBoolean(builder, "smoothBlending", "Determines if dynamic lighting smoothly blends as you move. Works best with a high refresh rate. Will result in worse performance since lighting updates typically happen more often.", true);
			this.threadedRendering = ConfigBuilder.makeBoolean(builder, "threadedRendering", "If enabled, chunks with dynamic lighting will always render off thread. Improves FPS, but may cause strange rendering glitches (Does nothing if Optifine is installed).", true);

			builder.push("entity_lighting");
			this.entityBrightnessOverrides = ConfigBuilder.makeList(builder, "entityBrightnessOverrides", "Sets the light level of entities. Allows for adding modded entities. Set the light to -1 to unregister it.", List.of("minecraft:blaze=9", "minecraft:creeper=5"), List.of(), o -> true);
			builder.push("burning_entities");
			this.burningEntitiesGlow = ConfigBuilder.makeBoolean(builder, "burningEntitiesGlow", "Determines if entities on fire emit light.", true);
			this.burningEntityLightLevel = ConfigBuilder.makeInt(builder, "burningEntityLightLevel", "How bright a burning entity should be.", 10, 0, 15);
			builder.pop();
			builder.push("glowing_entities");
			this.glowingEntitiesGlow = ConfigBuilder.makeBoolean(builder, "glowingEntitiesGlow", "Determines if entities affected with the glowing effect emit light.", true);
			this.glowingEntityLightLevel = ConfigBuilder.makeInt(builder, "glowingEntityLightLevel", "How bright an entity with the glowing effect be.", 5, 0, 15);
			builder.pop();
			builder.push("entity_settings");
			this.creepersGlow = ConfigBuilder.makeBoolean(builder, "creepersGlow", "Determines if if creepers emit light before they explode.", true);
			builder.pop();
			builder.pop();

			builder.push("item_lighting");
			this.itemBrightnessOverrides = ConfigBuilder.makeList(builder, "itemBrightnessOverrides", "Modifies the light level of items. Allows for adding modded items. Set the light to -1 to unregister it.", List.of("minecraft:torch=14", "minecraft:diamond=10"), List.of(), o -> true);
			this.itemEntitiesGlow = ConfigBuilder.makeBoolean(builder, "itemEntitiesGlow", "Determines if items in the world emit light.", true);
			this.heldItemsGlow = ConfigBuilder.makeBoolean(builder, "heldItemsGlow", "Determines if held or worn items emit light.", true);
			this.itemFramesGlow = ConfigBuilder.makeBoolean(builder, "itemFramesGlow", "Determines if item frames with glowing items emit light.", true);
			builder.pop();
			builder.pop();

			// Emissive blocks
			builder.push("block_emission_settings");
			this.blockTexturesGlow = ConfigBuilder.makeBoolean(builder, "blockTexturesGlow", "Determines if block textures can be lit brighter than usual.", true);
			this.blockTextureBrightnessOverrides = ConfigBuilder.makeList(builder, "blockTextureBrightnessOverrides", "Modifies the light level of block textures. Set the light to -1 to unregister it.", List.of("minecraft:block/end_portal_frame_eye=12", "minecraft:block/stone=4"), List.of(), o -> true);
			builder.pop();

			// Emissive armor trims
			builder.push("armor_trim_emission_settings");
			this.armorTrimsGlow = ConfigBuilder.makeBoolean(builder, "armorTrimsGlow", "Determines if armor trim materials can be lit brighter than usual.", true);
			this.armorTrimBrightnessOverrides = ConfigBuilder.makeList(builder, "armorTrimBrightnessOverrides", "Modifies the light level of armor trim textures based on trim material. Set the light to -1 to unregister it.", List.of("minecraft:diamond=10", "minecraft:gold=7"), List.of(), o -> true);
			builder.pop();

			// Debug
			builder.push("debug_settings");
			this.logRegistry = ConfigBuilder.makeBoolean(builder, "logRegistry", "Logs info about registry data.", false);
			this.logLightCalculationTime = ConfigBuilder.makeBoolean(builder, "logLightCalculationTime", "Logs the average time 100 dynamic lighting calculations took, and provides a fastest refresh time for your machine based on these numbers.", false);
			this.logDeprecatedFolderStructure = ConfigBuilder.makeBoolean(builder, "logDeprecatedFolderStructure", "Logs when a mod or resource pack is using a deprecated folder structure.", true);
			this.logDynamicLightingErrors = ConfigBuilder.makeBoolean(builder, "logDynamicLightingErrors", "Logs when dynamic lighting encounters an issue.", false);
			builder.push("flags");
			this.disableBlockTextureGlowIfError = ConfigBuilder.makeBoolean(builder, "disableBlockTextureGlowIfError", "Disables emissive block textures if an incompatible mod is discovered.", true);
			this.warnAboutFlywheel = ConfigBuilder.makeBoolean(builder, "warnAboutFlywheel", "Warns when Flywheel is installed since you may see weird dynamic lighting on some blocks from Create.", true);
			this.warnAboutSodium = ConfigBuilder.makeBoolean(builder, "warnAboutSodium", "Warns when Sodium (or a variation) is installed since dynamic lighting may not work.", true);
			builder.pop();
			builder.pop();

			// Compat
			builder.push("mod_compat");
			builder.push("enabled_builtin_plugins");
			this.blueSkiesPlugin = pluginConfig(builder, "blue_skies", "Blue Skies");
			this.createPlugin = pluginConfig(builder, "create", "Create");
			this.lavaMonsterPlugin = pluginConfig(builder, "lava_monster", "Lava Monster");
			this.nethercraftPlugin = pluginConfig(builder, "nethercraft", "Nethercraft");
			this.quarkPlugin = pluginConfig(builder, "quark", "Quark");
			this.tinkersConstructPlugin = pluginConfig(builder, "tconstruct", "Tinkers' Construct");
			builder.pop();
			builder.pop();
		}

		private void reload()
		{
			Minecraft mc = Minecraft.getInstance();
			if (mc.level != null)
				LucentRegistry.registerData();
		}

		private static Optional<ModConfigSpec.BooleanValue> pluginConfig(ModConfigSpec.Builder builder, String modID, String modName)
		{
			if (ModList.get().isLoaded(modID))
				return Optional.of(ConfigBuilder.makeBoolean(builder, modID, "Enables the plugin for " + modName + ".", true));
			return Optional.empty();
		}
	}

	public static void detectEnvironment()
	{
		ModList modList = ModList.get();
		if (CLIENT.blockTexturesGlow.get() && CLIENT.disableBlockTextureGlowIfError.get())
		{
			List<String> incompat = List.<String>of().stream().filter(modList::isLoaded).toList();
			int size = incompat.size();
			if (size > 0)
			{
				StringBuilder str = new StringBuilder();
				for (int i = 0; i < size; i++)
				{
					if (size > 1)
					{
						if (i > 0 && size > 2)
							str.append(", ");
						if (i == size - 1)
							str.append(" and ");
					}
					str.append(incompat.get(i));
				}
				oneTimeWarning("You are currently running " + str + ". Turning off emissive block textures to prevent visual issues. You may re-enable this feature in the config.");
				CLIENT.disableBlockTextureGlowIfError.set(false);
				CLIENT.blockTexturesGlow.set(false);
			}
		}
		// 1.20.1, works with embeddium but not rubidium
		if (CLIENT.warnAboutSodium.get() && (modList.isLoaded("rubidium") || modList.isLoaded("rubidium")) && !modList.isLoaded("embeddium"))
		{
			oneTimeWarning("You are currently running Rubidium. Lucent is incompatible with Rubidium as dynamic lighting will not function. Lucent does work with Embeddium however (last checked in 1.20.1).");
			CLIENT.warnAboutSodium.set(false);
		}
		if (CLIENT.warnAboutFlywheel.get() && modList.isLoaded("flywheel"))
		{
			String affected = modList.isLoaded("create") ? "Some blocks from Create may have incorrect dynamic lighting effects" : "Some blocks may have incorrect dynamic lighting effects";
			String performanceImpact = modList.isLoaded("create") ? "Note that doing so may impact performance for Create." : "Note that doing so may impact performance for mods using it.";
			oneTimeWarning("You are currently running flyweel. " + affected + ". If you want to avoid this, you can turn the \"backend\" config option in flyweel to \"OFF\". " + performanceImpact);
			CLIENT.warnAboutFlywheel.set(false);
		}
	}

	private static void oneTimeWarning(String message)
	{
		warning(message + " This is a one-time warning.");
	}

	private static void warning(String message)
	{
		Minecraft mc = Minecraft.getInstance();
		mc.gui.getChat().addMessage(Component.literal("[Lucent] ").withStyle(ChatFormatting.GOLD).append(Component.literal(message).withStyle(ChatFormatting.GRAY)));
	}
}
