package com.legacy.lucent.core.asm_hooks;

import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;

import net.minecraft.client.Minecraft;
import net.minecraft.core.SectionPos;

public class LevelRendererHooks
{
	private static Forced forced = Forced.NONE;
	private static int syncBelowCounter = 0;

	@SuppressWarnings("resource")
	public static void setForcedThreadedRender(SectionPos section)
	{
		if (LucentConfig.CLIENT.threadedRendering.get() && DynamicLightingEngine.sectionNeedsUpdate(section))
		{
			forced = Forced.THREADED;
			if (section.equals(SectionPos.of(Minecraft.getInstance().gameRenderer.getMainCamera().getBlockPosition()).below()))
			{
				if (LucentConfig.CLIENT.lightRefreshRate.getConfigValue().get() > 9 && syncBelowCounter++ == LucentConfig.CLIENT.lightRefreshRate.getConfigValue().get() / 10)
				{
					forced = Forced.SYNCED;
					syncBelowCounter = 0;
				}
			}
		}
	}

	public static boolean isSyncedRender(boolean flag)
	{
		Forced old = forced;
		forced = Forced.NONE;
		return old == Forced.THREADED ? false : (old == Forced.SYNCED || flag);
	}

	private static enum Forced
	{
		NONE,
		THREADED,
		SYNCED;
	}
}
