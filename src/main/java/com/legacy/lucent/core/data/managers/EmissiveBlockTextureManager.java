package com.legacy.lucent.core.data.managers;

import com.legacy.lucent.api.data.objects.EmissiveBlockTexture;

import net.minecraft.resources.ResourceLocation;

public class EmissiveBlockTextureManager extends LucentDataManager<ResourceLocation, EmissiveBlockTexture>
{
	public EmissiveBlockTextureManager()
	{
		super(EmissiveBlockTexture.TYPE);
	}
}