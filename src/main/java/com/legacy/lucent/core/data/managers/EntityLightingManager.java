package com.legacy.lucent.core.data.managers;

import com.legacy.lucent.api.data.objects.EntityLighting;

import net.minecraft.resources.ResourceLocation;

public class EntityLightingManager extends LucentDataManager<ResourceLocation, EntityLighting<?>>
{
	public EntityLightingManager()
	{
		super(EntityLighting.TYPE);
	}
}
