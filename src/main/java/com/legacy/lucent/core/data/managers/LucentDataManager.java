package com.legacy.lucent.core.data.managers;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.legacy.lucent.core.LucentMod;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.JsonOps;

import net.minecraft.resources.FileToIdConverter;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.GsonHelper;
import net.minecraft.util.profiling.ProfilerFiller;

public abstract class LucentDataManager<K, V> extends SimplePreparableReloadListener<Map<ResourceLocation, List<JsonElement>>>
{
	protected static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
	public static final Logger LOGGER = LucentMod.LOGGER.getLogger();
	protected final DatapackType<K, V> type;
	protected BiMap<K, V> data = HashBiMap.create();
	private final String directory;
	private final String oldDirectory;

	public LucentDataManager(DatapackType<K, V> type)
	{
		this.type = type;
		this.directory = LucentMod.MODID + "_data/" + type.folder();
		this.oldDirectory = LucentMod.MODID + "/" + type.folder();
	}

	private Map<ResourceLocation, List<Resource>> listResourceStacks(ResourceManager resourceManager, FileToIdConverter fileToId, FileToIdConverter fileToIdOld)
	{
		boolean canMerge = DatapackType.Mergable.class.isAssignableFrom(this.type.valueClass());
		Map<ResourceLocation, List<Resource>> data = canMerge ? fileToId.listMatchingResourceStacks(resourceManager) : fileToId.listMatchingResources(resourceManager).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> List.of(e.getValue())));

		// TODO 1.21 this when we start enforcing "lucent_data" as the folder instead of "lucent". We can simply return `data` then.
		Map<ResourceLocation, List<Resource>> old = canMerge ? fileToIdOld.listMatchingResourceStacks(resourceManager) : fileToIdOld.listMatchingResources(resourceManager).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> List.of(e.getValue())));
		Map<ResourceLocation, List<Resource>> ret = new HashMap<>();
		data.forEach((rl, l) ->
		{
			ret.computeIfAbsent(rl, k -> new ArrayList<>()).addAll(l);
		});
		old.forEach((rl, l) ->
		{
			LOGGER.warn(rl + " is using a deprecated resource pack folder structure. Use \"" + rl.toString().replace(this.oldDirectory, this.directory) + "\" instead. Using the old structure will no longer work in 1.21.");
			ResourceLocation convertedName = rl.withPath(rl.getPath().replace(this.oldDirectory, this.directory));
			ret.computeIfAbsent(convertedName, k -> new ArrayList<>()).addAll(l);
		});
		return ret;
	}

	@Override
	protected Map<ResourceLocation, List<JsonElement>> prepare(ResourceManager resourceManager, ProfilerFiller profilerFiller)
	{
		Map<ResourceLocation, List<JsonElement>> map = Maps.newHashMap();
		FileToIdConverter fileToId = FileToIdConverter.json(this.directory);
		FileToIdConverter fileToIdOld = FileToIdConverter.json(this.oldDirectory);

		for (Map.Entry<ResourceLocation, List<Resource>> entry : this.listResourceStacks(resourceManager, fileToId, fileToIdOld).entrySet())
		{
			ResourceLocation fullLocation = entry.getKey();
			ResourceLocation id = fileToId.fileToId(fullLocation);
			try
			{
				List<JsonElement> jsons = new ArrayList<>(entry.getValue().size());
				for (Resource resource : entry.getValue())
				{
					try (Reader reader = resource.openAsReader())
					{
						JsonElement json = GsonHelper.fromJson(GSON, reader, JsonElement.class);
						jsons.add(json);
					}
				}
				map.put(id, jsons);
			}
			catch (IllegalArgumentException | IOException | JsonParseException e)
			{
				LOGGER.error("Couldn't parse data file {} from {}", id, fullLocation, e);
			}
		}
		return map;
	}

	/**
	 * Handles going through the data and parsing it, storing it into the data map
	 * when finished
	 */
	@Override
	protected void apply(Map<ResourceLocation, List<JsonElement>> jsonMap, ResourceManager resourceManager, ProfilerFiller profiler)
	{
		BiMap<K, V> newDataMap = HashBiMap.create();
		String folderName = this.type.folder();

		for (Entry<ResourceLocation, List<JsonElement>> entry : jsonMap.entrySet())
		{
			ResourceLocation fileName = entry.getKey();

			try
			{
				for (JsonElement json : entry.getValue())
				{
					V val = this.type.valueCodec().decode(JsonOps.INSTANCE, json.getAsJsonObject()).getOrThrow(false, JsonParseException::new).getFirst();
					K key = this.type.keyCompute().apply(fileName, val);
					Pair<K, V> parsedData = Pair.of(key, val);
					this.registerData(newDataMap, parsedData.getFirst(), parsedData.getSecond());
				}
			}
			catch (Exception e)
			{
				LOGGER.error("Parsing error loading {}: {}", folderName, fileName, e);
			}
		}

		this.data = newDataMap;
		this.afterLoad();

		LOGGER.info("Loaded {} {}", newDataMap.size(), folderName);
	}

	/**
	 * Fires after all data is loaded, before data is frozen
	 */
	protected void afterLoad()
	{

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void registerData(BiMap<K, V> activeData, K key, V value)
	{
		V oldVal = activeData.get(key);
		if (oldVal instanceof DatapackType.Mergable<?> oldMergable && value instanceof DatapackType.Mergable<?> mergable && !mergable.override())
		{
			((DatapackType.Mergable) oldMergable).merge(mergable);
		}
		else
		{
			activeData.put(key, value);
		}
	}

	/**
	 * Forcefully injects data into the data manager.
	 * 
	 * @param registryData
	 */
	public void injectData(Map<K, V> registryData)
	{
		for (var entry : registryData.entrySet())
			this.registerData(this.data, entry.getKey(), entry.getValue());
		this.afterLoad();
	}

	public DatapackType<K, V> getType()
	{
		return this.type;
	}

	/**
	 * Get the data map for this instance
	 */
	public BiMap<K, V> getData()
	{
		return this.data;
	}

	/**
	 * Shorthand to get a value from the data map
	 */
	@Nullable
	public V getValue(K key)
	{
		return this.data.get(key);
	}

	/**
	 * Shorthand to get a key from the data map
	 */
	@Nullable
	public K getKey(V value)
	{
		return this.data.inverse().get(value);
	}

	public boolean containsKey(K key)
	{
		return key == null ? false : this.data.containsKey(key);
	}
	
	public String getDirectory()
	{
		return this.directory;
	}
}
