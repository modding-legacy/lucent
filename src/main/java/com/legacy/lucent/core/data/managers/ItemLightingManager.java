package com.legacy.lucent.core.data.managers;

import com.legacy.lucent.api.data.objects.ItemLighting;

import net.minecraft.resources.ResourceLocation;

public class ItemLightingManager extends LucentDataManager<ResourceLocation, ItemLighting>
{
	public ItemLightingManager()
	{
		super(ItemLighting.TYPE);
	}
}
