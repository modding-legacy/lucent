package com.legacy.lucent.core.data.managers;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimpleJsonResourceReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.neoforged.neoforge.client.event.RegisterClientReloadListenersEvent;

public class LucentAssets
{
	public static final EntityLightingManager ENTITY_LIGHTING = new EntityLightingManager();
	public static final ItemLightingManager ITEM_LIGHTING = new ItemLightingManager();
	public static final EmissiveBlockTextureManager EMISSIVE_BLOCK_TEXTURE = new EmissiveBlockTextureManager();
	public static final EmissiveArmorTrimTextureManager EMISSIVE_ARMOR_TRIM_TEXTURE = new EmissiveArmorTrimTextureManager();

	public static void registerListeners(final RegisterClientReloadListenersEvent event)
	{
		event.registerReloadListener(ENTITY_LIGHTING);
		event.registerReloadListener(ITEM_LIGHTING);
		event.registerReloadListener(EMISSIVE_BLOCK_TEXTURE);
		event.registerReloadListener(EMISSIVE_ARMOR_TRIM_TEXTURE);
		event.registerReloadListener(new DummyReload());
	}

	private static class DummyReload extends SimpleJsonResourceReloadListener
	{
		DummyReload()
		{
			super(new Gson(), LucentMod.MODID + "/dummy");
		}

		@SuppressWarnings("resource")
		@Override
		protected void apply(Map<ResourceLocation, JsonElement> data, ResourceManager resourceManager, ProfilerFiller profiler)
		{
			if (Minecraft.getInstance().level != null)
				LucentRegistry.registerData();
		}
	}
}
