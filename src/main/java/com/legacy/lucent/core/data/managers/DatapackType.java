package com.legacy.lucent.core.data.managers;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.random.WeightedEntry;
import net.minecraft.util.random.WeightedRandomList;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;

/**
 * folder = the folder the data is stored in.
 * <p>
 * keyCodec = The codec that handles the key for sending data over packets.
 * <p>
 * valueCodec = The codec that handles the value for sending data over packets,
 * reading from datapacks, and datagen.
 * <p>
 * keyCompute = Obtains the key that should be used in the SkiesDataProvider,
 * which keyCodec will use.
 * <p>
 * 
 * @author Silver_David
 *
 * @param <K>
 * @param <V>
 */
@SuppressWarnings("deprecation")
public record DatapackType<K, V>(Class<? extends V> valueClass, String folder, Codec<K> keyCodec, Codec<V> valueCodec, BiFunction<ResourceLocation, V, K> keyCompute)
{

	public static <K, V, C extends V> DatapackType<K, V> type(Class<C> valueClass, String folder, Codec<K> keyCodec, Codec<V> valueCodec, BiFunction<ResourceLocation, V, K> keyCompute)
	{
		return new DatapackType<>(valueClass, folder, keyCodec, valueCodec, keyCompute);
	}

	public static <V, C extends V> DatapackType<ResourceLocation, V> byName(Class<C> valueClass, String folder, Codec<V> valueCodec)
	{
		return type(valueClass, folder, ResourceLocation.CODEC, valueCodec, (n, v) -> n);
	}

	public static <V, C extends V> DatapackType<Item, V> byItem(Class<C> valueClass, String folder, Codec<V> valueCodec, BiFunction<ResourceLocation, V, Item> keyCompute)
	{
		return type(valueClass, folder, BuiltInRegistries.ITEM.byNameCodec(), valueCodec, keyCompute);
	}

	public static <V, C extends V> DatapackType<Block, V> byBlock(Class<C> valueClass, String folder, Codec<V> valueCodec, BiFunction<ResourceLocation, V, Block> keyCompute)
	{
		return type(valueClass, folder, BuiltInRegistries.BLOCK.byNameCodec(), valueCodec, keyCompute);
	}

	public Codec<V> registryCodec(Supplier<LucentDataManager<K, V>> dataManager, Supplier<V> defaultValue)
	{
		return Codec.either(this.keyCodec, this.valueCodec).xmap(either ->
		{
			return either.map(key ->
			{
				if (key != null)
				{
					var val = dataManager.get().getValue(key);
					if (val != null)
						return val;
				}
				return defaultValue.get();
			}, Function.identity());
		}, value ->
		{
			var key = dataManager.get().getKey(value);
			return key != null ? Either.left(key) : Either.right(value);
		});
	}

	public static interface Mergable<V extends Mergable<V>>
	{
		/**
		 * Merges data into this object from the object passed. The object passed will
		 * not be registered.
		 * 
		 * @param from
		 *            Data to inject into this
		 */
		void merge(V from);

		/**
		 * @return True if this object should override an existing registry entry. False
		 *         if this object should attempt to merge into an existing object.
		 */
		boolean override();
		
		default <L extends WeightedEntry> WeightedRandomList<L> mergeWeightedLists(WeightedRandomList<L> a, WeightedRandomList<L> b)
		{
			return  WeightedRandomList.create(Stream.concat(a.unwrap().stream(), b.unwrap().stream()).toList());
		}
	}

	public static interface KeyBindable<K>
	{
		/**
		 * @param key
		 *            The value of the key for this object in registry
		 */
		void setKey(K key);

		/**
		 * @return The registry key of this object
		 */
		K getKey();

		/**
		 * Sets the key of this object, but only works if the key has not yet been set
		 * 
		 * @throws IllegalStateException
		 */
		default void bindKey(K key) throws IllegalStateException
		{
			if (this.getKey() == null)
				this.setKey(key);
			else
				throw new IllegalStateException("Tried to bind the key of " + this + " to " + key + ", but it was already set to " + this.getKey());
		}
	}
}
