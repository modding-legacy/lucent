package com.legacy.lucent.core.data.managers;

import com.legacy.lucent.api.data.objects.EmissiveArmorTrimTexture;

import net.minecraft.resources.ResourceLocation;

public class EmissiveArmorTrimTextureManager extends LucentDataManager<ResourceLocation, EmissiveArmorTrimTexture>
{
	public EmissiveArmorTrimTextureManager()
	{
		super(EmissiveArmorTrimTexture.TYPE);
	}
}
