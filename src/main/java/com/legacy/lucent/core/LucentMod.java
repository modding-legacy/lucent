package com.legacy.lucent.core;

import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.DetectedVersion;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.IExtensionPoint;
import net.neoforged.fml.ModList;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.data.event.GatherDataEvent;

/**
 * A dynamic lighting API for Minecraft. Please reference the api package for
 * anything that you'd need to use as a modder.
 * 
 * Note: This is an API. Please do not just copy the code into your own mod. The
 * API exists for a reason.
 * https://maven.moddinglegacy.com/service/rest/repository/browse/maven-public/com/legacy/lucent/
 * 
 * @author Silver_David
 *
 */
@Mod(value = LucentMod.MODID)
public class LucentMod
{
	public static final String MODID = "lucent";
	public static final LoggerWrapper LOGGER = new LoggerWrapper(MODID);
	public static final Lazy<IEventBus> EVENT_BUS = Lazy.of(() -> ModList.get().getModContainerById(MODID).get().getEventBus());

	public LucentMod(IEventBus modBus)
	{
		ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> "drink water", (remote, isServer) -> true));
		if (FMLEnvironment.dist == Dist.CLIENT)
			com.legacy.lucent.core.LucentClient.init(modBus);
		if (FMLEnvironment.dist == Dist.DEDICATED_SERVER)
			LOGGER.warn("LUCENT IS A CLIENT SIDE MOD. IT DOES NOT NEED TO BE INSTALLED ON THE SERVER.");
		modBus.addListener(LucentMod::gatherData);
	}

	public static ResourceLocation locate(String path)
	{
		return new ResourceLocation(MODID, path);
	}

	protected static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();

		/*event.getGenerator().addProvider(true, new ItemLightingProvider(event)
		{
			@Override
			protected void gatherData()
			{
				this.register("test_lighting", ItemLighting.builder().item(Items.ACACIA_BOAT).light(Blocks.GLOWSTONE).worksOnLand(false).build());
			}
		});*/
		gen.addProvider(event.includeServer(), packMcmeta(output, "Lucent resources"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE))));
	}

	public static final class LoggerWrapper
	{
		private final Logger logger;
		private final String prefix;

		public LoggerWrapper(String modID)
		{
			modID = modID.toLowerCase(Locale.ENGLISH);
			StringBuilder modProper = new StringBuilder();
			for (String word : modID.split("_"))
				modProper.append(word.substring(0, 1).toUpperCase(Locale.ENGLISH)).append(word.substring(1));

			this.logger = LogManager.getLogger("ModdingLegacy/" + modProper);
			this.prefix = "[" + modID + "] ";
		}

		private void log(Level level, Object message, Object... params)
		{
			this.logger.log(level, this.prefix + message, params);
		}

		public void info(Object message, Object... params)
		{
			this.log(Level.INFO, message, params);
		}

		public void debug(Object message, Object... params)
		{
			this.log(Level.DEBUG, message, params);
		}

		public void error(Object message, Object... params)
		{
			this.log(Level.ERROR, message, params);
		}

		public void warn(Object message, Object... params)
		{
			this.log(Level.WARN, message, params);
		}

		public void fatal(Object message, Object... params)
		{
			this.log(Level.FATAL, message, params);
		}

		public void printStacktrace(String customMessage, Throwable t)
		{
			this.error(customMessage + " " + t.toString());
			var st = t.getStackTrace();
			for (var s : st)
				this.error("  " + s);
		}

		public Logger getLogger()
		{
			return this.logger;
		}
	}
}
