package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Pseudo;
import org.spongepowered.asm.mixin.injection.At;

import com.legacy.lucent.core.LucentClient;
import com.legacy.lucent.core.capability.IEntityLightingData;
import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;
import com.legacy.lucent.core.dynamic_lighting.LightData;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Tells the entity about the dynamic light level of the location in question so
 * it can be lit properly.
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
@Pseudo
@Mixin(EntityRenderer.class)
public class EntityRendererMixin
{	
	@ModifyReturnValue(at = @At(value = "RETURN"), method = "getPackedLightCoords(Lnet/minecraft/world/entity/Entity;F)I")
	private int modifyLightValue(int original, Entity entity, float eyePos)
	{
		BlockPos pos = BlockPos.containing(entity.getLightProbePosition(eyePos));
		LightData lightData = DynamicLightingEngine.getLightSource(pos);
		if (lightData != null)
		{	
			float blockLight = Math.max(DynamicLightingEngine.calcBlockLight(entity.level(), pos, lightData), ((IEntityLightingData) entity).lucent$getBrightness());
			if (blockLight > LightTexture.block(original))
			{
				return LucentClient.packLight(blockLight, LightTexture.sky(original));
			}
		}
		return original;
	}
}
