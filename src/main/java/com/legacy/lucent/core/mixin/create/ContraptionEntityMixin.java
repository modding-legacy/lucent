package com.legacy.lucent.core.mixin.create;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Pseudo;

import com.legacy.lucent.core.compat.create.IContraptionEntityInfo;
import com.legacy.lucent.core.plugin.CreatePlugin;

// This code could break in the future if Create ever moves this class.
@Pseudo
@Mixin(targets = "com.simibubi.create.content.contraptions.AbstractContraptionEntity", remap = false)
public class ContraptionEntityMixin implements IContraptionEntityInfo
{
	@Nullable
	private CreatePlugin.Data lucent$data = null;
	private int lucent$readAttempts = 0;

	@Override
	@Nullable
	public CreatePlugin.Data lucent$getLightData()
	{
		return lucent$data;
	}

	@Override
	public void lucent$setLightData(@Nullable CreatePlugin.Data data)
	{
		lucent$data = data;
	}

	@Override
	public boolean lucent$shouldAttemptRead()
	{
		return lucent$data == null && lucent$readAttempts < 10;
	}

	@Override
	public void lucent$incrementReadAttempt()
	{
		lucent$readAttempts++;
	}
}
