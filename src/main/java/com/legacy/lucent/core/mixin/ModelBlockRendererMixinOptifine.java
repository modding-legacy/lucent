package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.lucent.core.block_emission.BlockEmissionEngine;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.block.ModelBlockRenderer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Modifies the brightness of a quad for emissive block rendering.
 * 
 * Optifine changes a method signature and builds with the mcp mapped name.
 * Adjusting the signature, using that name, and setting remap to false will
 * work with Optifine on build.
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
@Mixin(value = ModelBlockRenderer.class, priority = 50)
public class ModelBlockRendererMixinOptifine
{
	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 0, remap = false, method = "renderQuadSmooth(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIIILnet/optifine/render/RenderEnv;)V")
	private int light0(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 1, remap = false, method = "renderQuadSmooth(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIIILnet/optifine/render/RenderEnv;)V")
	private int light1(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 2, remap = false, method = "renderQuadSmooth(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIIILnet/optifine/render/RenderEnv;)V")
	private int light2(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 3, remap = false, method = "renderQuadSmooth(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIIILnet/optifine/render/RenderEnv;)V")
	private int light3(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}
}
