package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;

import com.legacy.lucent.core.capability.IEntityLightingData;

import net.minecraft.world.entity.Entity;

@Mixin(Entity.class)
public class EntityMixin implements IEntityLightingData
{
	private int lucent$brightness = 0;
	
	@Override
	public int lucent$getBrightness()
	{
		return lucent$brightness;
	}
	
	@Override
	public void lucent$setBrightness(int brightness)
	{
		lucent$brightness = brightness;
	}
}
