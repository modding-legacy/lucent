package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.lucent.core.LucentClient;
import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;
import com.legacy.lucent.core.dynamic_lighting.LightData;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Tells the world renderer to display the light level from the dynamic lighting
 * engine.
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
@Mixin(LevelRenderer.class)
public class LevelRendererMixin
{
	@ModifyReturnValue(at = @At(value = "RETURN"), method = "getLightColor(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;)I")
	private static int modifyBlockLight(int original, BlockAndTintGetter level, BlockState state, BlockPos pos)
	{
		LightData lightData = DynamicLightingEngine.getLightSource(pos);
		if (lightData != null)
		{
			float blockLight = Math.max(DynamicLightingEngine.calcBlockLight(level, pos, lightData), state.getLightEmission(level, pos));
			if (blockLight > LightTexture.block(original))
			{
				return LucentClient.packLight(blockLight, LightTexture.sky(original));
			}
		}
		return original;
	}

	@Inject(at = @At(value = "HEAD"), method = "allChanged()V")
	private void onRenderAll(CallbackInfo callback)
	{
		DynamicLightingEngine.clearAllData();
	}
}
