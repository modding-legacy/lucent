package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.lucent.core.block_emission.BlockEmissionEngine;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.block.ModelBlockRenderer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Modifies the brightness of a quad for emissive block rendering.
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
@Mixin(value = ModelBlockRenderer.class, priority = 50)
public class ModelBlockRendererMixin
{
	// LIGHTING BLOCKS IN WORLD
	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 0, method = "putQuadData(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIII)V")
	private int light0(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 1, method = "putQuadData(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIII)V")
	private int light1(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 2, method = "putQuadData(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIII)V")
	private int light2(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}

	@ModifyVariable(at = @At(value = "HEAD"), ordinal = 3, method = "putQuadData(Lnet/minecraft/world/level/BlockAndTintGetter;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lcom/mojang/blaze3d/vertex/PoseStack$Pose;Lnet/minecraft/client/renderer/block/model/BakedQuad;FFFFIIIII)V")
	private int light3(int original, BlockAndTintGetter level, BlockState state, BlockPos pos, VertexConsumer vertexBuilder, PoseStack.Pose poseStack, BakedQuad quad)
	{
		return BlockEmissionEngine.calcLight(original, quad);
	}
}
