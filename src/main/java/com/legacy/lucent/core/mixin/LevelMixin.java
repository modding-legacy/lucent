package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Hooks into placing a block to manually add a light source when a block is
 * broken.
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
@Mixin(Level.class)
public class LevelMixin
{
	@Inject(at = @At(value = "RETURN"), method = "setBlock(Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/level/block/state/BlockState;II)Z")
	private void updateLightOnBlockChange(BlockPos pos, BlockState state, int flags, int updates, CallbackInfoReturnable<Boolean> callback)
	{
		if (callback.getReturnValue())
			DynamicLightingEngine.blockChanged(pos, state);
	}
}
