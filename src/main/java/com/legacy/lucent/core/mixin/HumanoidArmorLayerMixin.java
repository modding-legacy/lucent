package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.lucent.api.registry.ArmorTrimLightingRegistry;
import com.legacy.lucent.core.LucentConfig;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.armortrim.ArmorTrim;

@Mixin(HumanoidArmorLayer.class)
public class HumanoidArmorLayerMixin
{
	// Make full-bright armor trim textures
	@ModifyVariable(at = @At("HEAD"), ordinal = 0, method = "renderTrim(Lnet/minecraft/world/item/ArmorMaterial;Lcom/mojang/blaze3d/vertex/PoseStack;Lnet/minecraft/client/renderer/MultiBufferSource;ILnet/minecraft/world/item/armortrim/ArmorTrim;Lnet/minecraft/client/model/Model;Z)V", remap = false) // Patched by Forge
	private int modifyLightForgeMethod(int original, ArmorMaterial pArmorMaterial, PoseStack pPoseStack, MultiBufferSource pBuffer, int pPackedLight, ArmorTrim pTrim)
	{
		if (LucentConfig.CLIENT.armorTrimsGlow.get())
		{
			int customBlockLight = ArmorTrimLightingRegistry.get(pTrim);
			if (customBlockLight > -1)
				return LightTexture.pack(Math.max(customBlockLight, LightTexture.block(original)), LightTexture.sky(original));
		}
		return original;
	}

	// Make full-bright armor trim textures
	// Not called in 1.20.1, and loops itself. But if it ever works, this is ready
	@ModifyVariable(at = @At("HEAD"), ordinal = 0, method = "renderTrim(Lnet/minecraft/world/item/ArmorMaterial;Lcom/mojang/blaze3d/vertex/PoseStack;Lnet/minecraft/client/renderer/MultiBufferSource;ILnet/minecraft/world/item/armortrim/ArmorTrim;Lnet/minecraft/client/model/HumanoidModel;Z)V")
	private int modifyLightVanillaMethod(int original, ArmorMaterial pArmorMaterial, PoseStack pPoseStack, MultiBufferSource pBuffer, int pPackedLight, ArmorTrim pTrim)
	{
		if (LucentConfig.CLIENT.armorTrimsGlow.get())
		{
			int customBlockLight = ArmorTrimLightingRegistry.get(pTrim);
			if (customBlockLight > -1)
				return LightTexture.pack(Math.max(customBlockLight, LightTexture.block(original)), LightTexture.sky(original));
		}
		return original;
	}
}