package com.legacy.lucent.core.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.lucent.core.asm_hooks.LevelRendererHooks;

import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.core.SectionPos;

/**
 * Controls which chunks should be render on the Chunk Update thread.
 * 
 * @author David
 *
 */
@Mixin(LevelRenderer.class)
public class LevelRendererMixinThreadedRendering
{
	/*
	 * When getting a pos to render, store that we should render off thread if the config allows it and dynamic lighting is occuring
	 */
	@ModifyVariable(at = @At(value = "STORE", ordinal = 0), ordinal = 0, method = "compileSections(Lnet/minecraft/client/Camera;)V")
	private SectionPos onStoreRenderChunk(SectionPos renderSection)
	{
		LevelRendererHooks.setForcedThreadedRender(renderSection);
		return renderSection;
	}

	/*
	 * When storing flag on the line with the distance check, check compileThreadedRender
	 */
	@ModifyVariable(at = @At(value = "LOAD", ordinal = 0), ordinal = 0, method = "compileSections(Lnet/minecraft/client/Camera;)V")
	private boolean loadFlag(boolean flag)
	{
		return LevelRendererHooks.isSyncedRender(flag);
	}
}
