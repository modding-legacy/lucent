package com.legacy.lucent.core.mixin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.MixinEnvironment;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import com.legacy.lucent.core.LucentMod;

public class LucentMixinPlugin implements IMixinConfigPlugin
{
	@Override
	public void onLoad(String mixinPackage)
	{
	}

	@Override
	public String getRefMapperConfig()
	{
		return null;
	}

	@Override
	public boolean shouldApplyMixin(String targetClassName, String mixinClassName)
	{
		return true;
	}

	@Override
	public void acceptTargets(Set<String> myTargets, Set<String> otherTargets)
	{
	}

	@Override
	public List<String> getMixins()
	{		
		List<String> mixins = new ArrayList<>();
		if (MixinEnvironment.getCurrentEnvironment().getSide() == MixinEnvironment.Side.CLIENT)
		{
			mixins.addAll(Arrays.asList("EntityRendererMixin", "LevelRendererMixin", "LevelMixin", "EntityMixin"));

			// An attempt at Optifine compat
			if (doesClassExist("net.optifine.Lang"))
			{
				LucentMod.LOGGER.info("Found Optifine");
				mixins.addAll(Arrays.asList("ModelBlockRendererMixinOptifine"));
			}
			else // Things that crash or won't work with Optifine
			{
				mixins.addAll(Arrays.asList("ModelBlockRendererMixin", "LevelRendererMixinThreadedRendering", "HumanoidArmorLayerMixin"));
			}

			// Create compat
			// Yes. I know using a Mixin into another mod is incredibly hacky, and some of this could _probably_ be made into a capability. Since this is client side only, and I don't modify any data in Create, only appending, this should have next to no risk.
			if (doesClassExist("com.simibubi.create.compat.Mods")) // Using this class since it seems the least likley to break and won't load anything significant
			{
				LucentMod.LOGGER.info("Found Create");
				mixins.addAll(Arrays.asList("create.ContraptionEntityMixin"));
			}
		}
		return mixins;
	}

	private static boolean doesClassExist(String classPath)
	{
		try
		{
			Class.forName(classPath);
			return true;
		}
		catch (Throwable t)
		{
			return false;
		}
	}

	@Override
	public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
	{
	}

	@Override
	public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
	{
	}
}
