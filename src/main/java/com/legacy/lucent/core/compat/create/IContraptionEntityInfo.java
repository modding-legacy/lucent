package com.legacy.lucent.core.compat.create;

import javax.annotation.Nullable;

import com.legacy.lucent.core.plugin.CreatePlugin;

public interface IContraptionEntityInfo
{
	@Nullable
	CreatePlugin.Data lucent$getLightData();

	void lucent$setLightData(@Nullable CreatePlugin.Data data);

	boolean lucent$shouldAttemptRead();

	void lucent$incrementReadAttempt();
}
