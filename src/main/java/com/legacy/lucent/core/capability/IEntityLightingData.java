package com.legacy.lucent.core.capability;

/**
 * Stores the amount of light an entity is emitting. Used when rendering the
 * actual entity to prevent flickering
 * 
 * @author Silver_David
 *
 */
public interface IEntityLightingData
{
	/**
	 * @return Block light level (0-15)
	 */
	default int lucent$getBrightness()
	{
		return 0;
	}

	/**
	 * @param brightness
	 *            Block light level (0-15)
	 */
	default void lucent$setBrightness(int brightness)
	{

	}
}
