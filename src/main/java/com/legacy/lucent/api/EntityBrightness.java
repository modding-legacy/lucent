package com.legacy.lucent.api;

import java.util.function.Consumer;

import javax.annotation.Nullable;

import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * An object that holds the light level an entity should emit. A light level of
 * 0 does nothing.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class EntityBrightness
{
	/**
	 * The current entity that we are getting the brightness for, stored as a global
	 * variable for areas that have no access to it.
	 */
	@Nullable
	public static Entity activeEntity = null;
	private int lightLevel = 0;

	public EntityBrightness()
	{
	}

	/**
	 * @return The entity to check
	 */
	public Entity getEntity()
	{
		return activeEntity;
	}

	/**
	 * @return The light level the entity should emit
	 */
	public int getLightLevel()
	{
		return this.lightLevel;
	}

	/**
	 * Sets the light level that the entity should emit if the new value is higher
	 * than the existing value
	 * 
	 * @param lightLevel
	 */
	public void setLightLevel(int lightLevel)
	{
		if (lightLevel > this.lightLevel)
			this.lightLevel = Mth.clamp(lightLevel, 0, 15);
	}

	/**
	 * Runs the passed consumer if the entity stored is an instance of the entity
	 * class passed
	 * 
	 * @param <E>
	 *            The entity class
	 * @param entityClass
	 *            The entity class to check for
	 * @param action
	 *            Runs if the entity is an instance of entityClass
	 */
	public <E extends Entity> void ifEntityIs(Class<E> entityClass, Consumer<E> action)
	{
		if (entityClass.isInstance(this.getEntity()))
			action.accept(entityClass.cast(this.getEntity()));
	}
}
