package com.legacy.lucent.api;

import java.util.Objects;

import com.legacy.lucent.api.plugin.ILucentPlugin;

import net.minecraft.Util;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;

/**
 * Determines the position where dynamic lighting should occur at.
 * 
 * @author Silver_David
 *
 */
public abstract class VecTransformer
{
	private static final Vec3[] AXIS_VECS = Util.make(() ->
	{
		Vec3[] vecs = new Vec3[3];
		vecs[0] = new Vec3(1, 0, 0);
		vecs[1] = new Vec3(0, 1, 0);
		vecs[2] = new Vec3(0, 0, 1);
		return vecs;
	});

	protected final Vec3 vec;

	public VecTransformer(Vec3 vec)
	{
		this.vec = vec;
	}

	/**
	 * Transforms the stored Vec3 according to the object's implementation. In some
	 * cases where the entity is required, such as {@link Facing} this may do
	 * nothing.
	 * 
	 * @return The transformed Vec3
	 */
	public abstract Vec3 apply();

	/**
	 * Transforms the stored Vec3 according to the object's implementation and moves
	 * it to the entity's position.
	 * 
	 * @param entity
	 * @return The transformed Vec3, moved to the entity's position.
	 */
	public Vec3 applyForEntity(Entity entity)
	{
		return this.apply().add(entity.position());
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof VecTransformer v && this.getClass() == v.getClass() && Objects.equals(this.vec, v.vec);
	}

	/**
	 * Rotates
	 * 
	 * @param vec
	 * @param angleDegrees
	 * @param axis
	 * @return
	 */
	public static Vec3 rotateVec(Vec3 vec, float angleDegrees, Axis axis)
	{
		double theta = Mth.DEG_TO_RAD * angleDegrees;
		Vec3 axisVec = AXIS_VECS[axis.ordinal()];
		double x = vec.x();
		double y = vec.y();
		double z = vec.z();
		double u = axisVec.x();
		double v = axisVec.y();
		double w = axisVec.z();
		double xPrime = u * (u * x + v * y + w * z) * (1 - Math.cos(theta)) + x * Math.cos(theta) + (-w * y + v * z) * Math.sin(theta);
		double yPrime = v * (u * x + v * y + w * z) * (1 - Math.cos(theta)) + y * Math.cos(theta) + (w * x - u * z) * Math.sin(theta);
		double zPrime = w * (u * x + v * y + w * z) * (1 - Math.cos(theta)) + z * Math.cos(theta) + (-v * x + u * y) * Math.sin(theta);
		return new Vec3(xPrime, yPrime, zPrime);
	}

	/**
	 * Represents the exact position passed into it. Used by
	 * {@linkplain ILucentPlugin#registerEntityLightSourcePositionGetter(com.legacy.lucent.api.registry.EntityLightSourcePosRegistry)
	 * ILucentPlugin.registerEntityLightSourcePositionGetter}
	 * 
	 * @author Silver_David
	 *
	 */
	public static class Direct extends VecTransformer
	{
		public Direct(Vec3 vec)
		{
			super(vec);
		}

		@Override
		public Vec3 apply()
		{
			return Vec3.ZERO;
		}
		
		@Override
		public Vec3 applyForEntity(Entity entity)
		{
			return this.apply().add(this.vec);
		}
	}

	/**
	 * Represents a relative position applied to the entity
	 * 
	 * @author Silver_David
	 *
	 */
	public static class Relative extends VecTransformer
	{
		public Relative(Vec3 vec)
		{
			super(vec);
		}

		public Relative(float x, float y, float z)
		{
			this(new Vec3(x, y, z));
		}

		@Override
		public Vec3 apply()
		{
			return this.vec;
		}
	}

	/**
	 * Represents a vector position based on where the entity is looking
	 * 
	 * @author Silver_David
	 *
	 */
	public static class Facing extends VecTransformer
	{
		private final Vec3 anchor;

		/**
		 * 
		 * @param vec
		 *            x=left, y=up, z=forward
		 * @param anchor
		 *            Offsets the final vector by this. Useful for having vectors based
		 *            on the eye position of an entity
		 */
		public Facing(Vec3 vec, Vec3 anchor)
		{
			super(vec);
			this.anchor = anchor;
		}

		public Facing(Vec3 vec)
		{
			this(vec, Vec3.ZERO);
		}

		public Facing(float left, float up, float forward)
		{
			this(new Vec3(left, up, forward));
		}

		@Override
		public Vec3 apply()
		{
			return this.vec.add(this.anchor);
		}

		@Override
		public Vec3 applyForEntity(Entity entity)
		{
			Vec2 lookVec = entity.getRotationVector();
			Vec3 pos = new PitchAndYaw(this.vec, lookVec.x, -lookVec.y).apply();
			return pos.add(this.anchor).add(entity.position());
		}

		@Override
		public boolean equals(Object obj)
		{
			return super.equals(obj) && obj instanceof Facing v && Objects.equals(this.anchor, v.anchor);
		}
	}

	/**
	 * Represents a position obtained by raytracing the blocks in the world forward
	 * from the entity's facing direction, and stopping at the first collision.
	 * 
	 * @author Silver_David
	 *
	 */
	public static class RaytracedFacing extends VecTransformer
	{
		private final Vec3 anchor;
		private final ClipContext.Block blockClip;
		private final ClipContext.Fluid fluidClip;

		/**
		 * 
		 * @param maxDist
		 *            x=left, y=up, z=forward
		 * @param anchor
		 *            Offsets the final vector by this. Useful for having vectors based
		 *            on the eye position of an entity
		 * @param blockClip
		 * @param fluidClip
		 */
		public RaytracedFacing(Vec3 maxDist, Vec3 anchor, ClipContext.Block blockClip, ClipContext.Fluid fluidClip)
		{
			super(maxDist);
			this.anchor = anchor;
			this.blockClip = blockClip;
			this.fluidClip = fluidClip;
		}

		/**
		 * 
		 * @param maxDist
		 *            Forward distance
		 * @param anchor
		 *            Offsets the final vector by this. Useful for having vectors based
		 *            on the eye position of an entity
		 */
		public RaytracedFacing(float maxDist, Vec3 anchor)
		{
			this(new Vec3(0, 0, maxDist), anchor);
		}
		
		public RaytracedFacing(Vec3 maxDist, Vec3 anchor)
		{
			this(maxDist, anchor, ClipContext.Block.VISUAL, ClipContext.Fluid.NONE);
		}

		@Override
		public Vec3 apply()
		{
			return this.vec.add(this.anchor);
		}

		@Override
		public Vec3 applyForEntity(Entity entity)
		{
			Vec3 dest = new Facing(this.vec, this.anchor).applyForEntity(entity);
			BlockHitResult hitResult = entity.level().clip(new ClipContext(entity.position().add(this.anchor), dest, this.blockClip, this.fluidClip, entity));
			return hitResult.getLocation().add(new Vec3(hitResult.getDirection().step()));
		}
	}

	/**
	 * Represents a relative position that can be rotated along any axis by a
	 * specific amount
	 * 
	 * @author Silver_David
	 *
	 */
	public static class Rotated extends VecTransformer
	{

		private final float angle;
		private final Direction.Axis axis;

		public Rotated(Vec3 vec, float angleDegrees, Direction.Axis axis)
		{
			super(vec);
			this.angle = angleDegrees;
			this.axis = axis;
		}

		@Override
		public Vec3 apply()
		{
			return rotateVec(this.vec, this.angle, this.axis);
		}

		@Override
		public boolean equals(Object obj)
		{
			return super.equals(obj) && obj instanceof Rotated v && this.axis == v.axis && this.angle == v.angle;
		}
	}

	/**
	 * Represents a relative position that will be rotated along the z and y axis
	 * using pitch and yaw respectively
	 * 
	 * @author Silver_David
	 *
	 */
	public static class PitchAndYaw extends VecTransformer
	{
		private final float pitch, yaw;
		private final Axis pitchAxis;

		public PitchAndYaw(Vec3 vec, float pitchDegrees, float yawDegrees)
		{
			this(vec, pitchDegrees, yawDegrees, Axis.X);
		}

		public PitchAndYaw(Vec3 vec, float pitchDegrees, float yawDegrees, Axis pitchAxis)
		{
			super(vec);
			this.pitch = pitchDegrees;
			this.yaw = yawDegrees;
			this.pitchAxis = pitchAxis;
		}

		@Override
		public Vec3 apply()
		{
			Vec3 vec = rotateVec(this.vec, this.pitch, this.pitchAxis);
			return rotateVec(vec, this.yaw, Axis.Y);
		}

		@Override
		public boolean equals(Object obj)
		{
			return super.equals(obj) && obj instanceof PitchAndYaw v && this.pitch == v.pitch && this.yaw == v.yaw && this.pitchAxis == v.pitchAxis;
		}
	}
}
