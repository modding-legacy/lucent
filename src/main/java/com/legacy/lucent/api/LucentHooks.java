package com.legacy.lucent.api;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.api.registry.BlockTextureLightingRegistry;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.block_emission.BlockEmissionEngine;
import com.legacy.lucent.core.dynamic_lighting.DynamicLightingEngine;
import com.legacy.lucent.core.dynamic_lighting.LightData;

import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.block.state.BlockState;

/**
 * Contains general hooks that can be used by any mod to integrate dynamic
 * lighting and block emission into their render pipeline.
 * <p>
 * The code in this class will not change unless something drastic changes in
 * how Minecraft handles lighting, so it will be safe to use. If any method here
 * should become unusable in the future due to new requirements, the method will
 * still exist, but simply do nothing. Such methods will be appropriately
 * deprecated.
 * <p>
 * Using this class assumes that you do not import any data, and reference it
 * through it's full class path to prevent code breaks if Lucent is not
 * installed. <code>
 * <br>{
 * <br>&emsp;if (ModList.get().isLoaded("lucent"))
 * <br>&emsp;&emsp;blockLight = com.legacy.lucent.api.LucentHooks.getBlockLight(level, state, pos, vanillaBlockLight);
 * <br>}
 * 
 * </code>
 * <p>
 * Any code that would behave more like an event, such as marking a chunk
 * section for render updates, is handled in {@link ILucentPlugin}
 * <p>
 * For anyone who uses this, the fact that you took the time to do so means the
 * world and I cannot thank you enough.
 * 
 * @author Silver_David
 *
 */
public class LucentHooks
{
	/**
	 * Obtains the light level of a given position from the dynamic lighting engine.
	 * This value is represented as a float for smoothness, but may be rounded if
	 * your system doesn't support it.
	 * 
	 * @param level
	 * @param state
	 * @param pos
	 *            The position to check
	 * @param vanillaBlockLight
	 *            The vanilla block light level
	 * @return The brightest light level chosen between the vanilla light level and
	 *         the dynamic lighting one.
	 */
	public static float getBlockLight(BlockAndTintGetter level, BlockState state, BlockPos pos, float vanillaBlockLight)
	{
		LightData lightData = DynamicLightingEngine.getLightSource(pos);
		if (lightData != null)
		{
			float blockLight = DynamicLightingEngine.calcBlockLight(level, pos, lightData);
			if (blockLight > vanillaBlockLight)
				return blockLight;
		}
		return vanillaBlockLight;
	}

	/**
	 * Obtains the packed light level (represented in vanilla terms per
	 * {@linkplain LevelRenderer#getLightColor(BlockAndTintGetter, BlockState, BlockPos)
	 * LevelRenderer.getLightColor}).
	 * 
	 * @param level
	 * @param state
	 * @param pos
	 * @param vanillaPackedLight
	 *            The vanilla packed light value
	 * @return The brightest packed light level chosen between the vanilla light
	 *         level and the dynamic lighting one.
	 */
	public static int getPackedLight(BlockAndTintGetter level, BlockState state, BlockPos pos, int vanillaPackedLight)
	{
		LightData lightData = DynamicLightingEngine.getLightSource(pos);
		if (lightData != null)
		{
			int light = DynamicLightingEngine.calcLight(level, state, pos, lightData);
			if (light > vanillaPackedLight)
				return light;
		}
		return vanillaPackedLight;
	}

	/**
	 * Obtains the block light level set for the texture name passed in.
	 * 
	 * @param textureName
	 * @param vanillaBlockLight
	 * @return The brightest light level chosen between the vanilla light level and
	 *         the emissive textures value
	 */
	public static float getTextureBlockLight(ResourceLocation textureName, int vanillaBlockLight)
	{
		return LucentConfig.CLIENT.blockTexturesGlow.get() ? Math.max(vanillaBlockLight, BlockTextureLightingRegistry.get(textureName)) : vanillaBlockLight;
	}

	/**
	 * Obtains the packed light level (represented in vanilla terms per
	 * {@linkplain LevelRenderer#getLightColor(BlockAndTintGetter, BlockState, BlockPos)
	 * LevelRenderer.getLightColor}) set for the texture name passed in.
	 * 
	 * @param textureName
	 * @param vanillaPackedLight
	 *            The original packed light level that would've been applied to this
	 *            texture
	 * @return The brightest packed light level chosen between the vanilla light
	 *         level and the emissive textures value
	 */
	public static int getTexturePackedLight(ResourceLocation textureName, int vanillaPackedLight)
	{
		return BlockEmissionEngine.calcLight(vanillaPackedLight, textureName);
	}
}
