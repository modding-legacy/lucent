package com.legacy.lucent.api.registry;

import com.legacy.lucent.api.data.objects.LightLevelProvider;
import com.legacy.lucent.api.data.objects.LightLevelProvider.LightLevelProviderType;

import net.minecraft.resources.ResourceLocation;

public class LightLevelProviderRegistry
{
	public <A extends LightLevelProvider<?>> LightLevelProviderType<A> register(ResourceLocation name, LightLevelProviderType<A> type)
	{
		return LightLevelProvider.LightLevelProviderType.register(name, type);
	}
}
