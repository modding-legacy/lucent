package com.legacy.lucent.api.registry;

import java.util.HashMap;
import java.util.Map;

import com.legacy.lucent.api.data.objects.EmissiveBlockTexture;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Holds the light level of block textures for emissive rendering.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class BlockTextureLightingRegistry
{
	/**
	 * Add textures through
	 * {@link ILucentPlugin#registerBlockTextureLightings(BlockTextureLightingRegistry)}
	 * on config load or when loading into a world.
	 * 
	 * @see #get(ResourceLocation)
	 */
	private static final Map<ResourceLocation, Integer> REGISTRY = new HashMap<>();

	public BlockTextureLightingRegistry()
	{
	}

	public void register(EmissiveBlockTexture emissiveTexture)
	{
		ResourceLocation texture = emissiveTexture.texture();
		int light = emissiveTexture.light();
		if (light <= -1)
		{
			this.remove(texture);
		}
		else
		{
			REGISTRY.put(texture, light);
			if (LucentConfig.CLIENT.logRegistry.get())
			{
				LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [" + texture + "] set to " + light);
			}
		}
	}

	public void register(ResourceLocation texture, int lightLevel)
	{
		this.register(new EmissiveBlockTexture(texture, lightLevel));
	}

	public void register(String texture, int lightLevel)
	{
		if (ResourceLocation.isValidResourceLocation(texture))
			this.register(new ResourceLocation(texture), lightLevel);
		else
			LucentRegistry.logInvalidResourceLocation(texture);
	}

	public void remove(ResourceLocation texture)
	{
		if (REGISTRY.remove(texture) != null && LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> Removed [" + texture + "]");
	}

	/**
	 * Gets the light of the passed texture file location
	 * 
	 * @param texture
	 * @return The light level of the texture. 0 by default
	 */
	public static int get(ResourceLocation texture)
	{
		Integer light = REGISTRY.get(texture);
		return light != null ? light : 0;
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clear()
	{
		REGISTRY.clear();
	}
}
