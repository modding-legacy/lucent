package com.legacy.lucent.api.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.lucent.api.data.objects.EntityLighting;
import com.legacy.lucent.api.data.objects.LightLevelProvider;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Holds the light levels of entities for dynamic lighting.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class EntityLightingRegistry
{
	/**
	 * Add entities through
	 * {@link ILucentPlugin#registerEntityLightings(EntityLightingRegistry)} on
	 * config load or when loading into a world
	 * 
	 * @see #get(Entity)
	 */
	private static final Map<EntityType<?>, List<EntityLighting<?>>> PROVIDER_REGISTRY = new HashMap<>();
	private static final List<EntityLighting<?>> DEFAULT_PROVIDERS = new ArrayList<>();
	private static Map<EntityType<?>, LucentRegistry.Source> registeredBy = new HashMap<>();

	public EntityLightingRegistry()
	{
	}

	public void register(EntityLighting<?> lighting)
	{
		LightLevelProvider<?> prov = lighting.provider();
		lighting.entity().ifPresentOrElse(entity ->
		{
			if (prov instanceof LightLevelProvider.Direct<?> direct && direct.value() <= -1)
			{
				this.remove(entity);
			}
			else
			{
				boolean overwrote = false;
				var source = registeredBy.get(entity);
				if (source != null && source.isBuiltin && source.priority < LucentRegistry.currentSource.priority)
				{
					List<EntityLighting<?>> list = new ArrayList<>();
					list.add(lighting);
					PROVIDER_REGISTRY.put(entity, list);
					overwrote = true;
				}
				else
				{
					PROVIDER_REGISTRY.computeIfAbsent(entity, i -> new ArrayList<>()).add(lighting);
				}
				registeredBy.put(entity, LucentRegistry.currentSource);
				if (LucentConfig.CLIENT.logRegistry.get())
				{
					LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [" + BuiltInRegistries.ENTITY_TYPE.getKey(entity) + "] " + (overwrote ? "overwrote " : "added ") + prov);
				}
			}
		}, () ->
		{
			DEFAULT_PROVIDERS.add(lighting);
			if (LucentConfig.CLIENT.logRegistry.get())
			{
				LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [all entities] added " + prov);
			}
		});
	}

	public void register(EntityLighting.Builder entityLightingBuilder)
	{
		if (!entityLightingBuilder.hasFailed())
			this.register(entityLightingBuilder.build());
	}

	public void register(@Nullable EntityType<?> entityType, LightLevelProvider<?> lightLevelProvider)
	{
		this.register(EntityLighting.builder().entity(entityType).light(lightLevelProvider));
	}

	public void register(LightLevelProvider<?> lightLevelProvider)
	{
		this.register(null, lightLevelProvider);
	}

	public void register(@Nullable EntityType<?> entityType, int lightLevel)
	{
		if (lightLevel <= -1)
			this.remove(entityType);
		else
			this.register(EntityLighting.builder().entity(entityType).light(new LightLevelProvider.Direct<>(lightLevel)));
	}

	public void register(ResourceLocation entityTypeId, int lightLevel)
	{
		if (BuiltInRegistries.ENTITY_TYPE.containsKey(entityTypeId))
			this.register(BuiltInRegistries.ENTITY_TYPE.get(entityTypeId), lightLevel);
		else if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + entityTypeId + "] was not found in registry");
	}

	public void register(String entityTypeId, int lightLevel)
	{
		if (ResourceLocation.isValidResourceLocation(entityTypeId))
			this.register(new ResourceLocation(entityTypeId), lightLevel);
		else
			LucentRegistry.logInvalidResourceLocation(entityTypeId);
	}

	/**
	 * @param <T>
	 * @param entityType
	 * @param lightLevel
	 *            A function to calculate the entity's light level. Set to null to
	 *            remove from registry.
	 */
	public <T extends Entity> void register(@Nullable EntityType<T> entityType, @Nullable Function<T, Integer> lightLevel)
	{
		if (lightLevel == null)
			this.remove(entityType);
		else
			this.register(EntityLighting.builder().entity(entityType).light(new LightLevelProvider.Custom<>(lightLevel)));
	}

	/**
	 * @param <T>
	 * @param lightFunction
	 *            A function to calculate the light level of any entity. Set to null
	 *            to remove from registry.
	 */
	public <T extends Entity> void register(@Nullable Function<T, Integer> lightFunction)
	{
		this.register((EntityType<T>) null, lightFunction);
	}

	/**
	 * @param <T>
	 * @param entityTypeId
	 * @param lightFunction
	 *            A function to calculate the light level of any entity. Set to null
	 *            to remove from registry.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> void register(ResourceLocation entityTypeId, @Nullable Function<T, Integer> lightFunction)
	{
		if (BuiltInRegistries.ENTITY_TYPE.containsKey(entityTypeId))
			this.register((EntityType<T>) BuiltInRegistries.ENTITY_TYPE.get(entityTypeId), lightFunction);
		else if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + entityTypeId + "] was not found in registry");
	}

	/**
	 * @param <T>
	 * @param entityTypeId
	 * @param lightFunction
	 *            A function to calculate the light level of any entity. Set to null
	 *            to remove from registry.
	 */
	public <T extends Entity> void register(String entityTypeId, @Nullable Function<T, Integer> lightFunction)
	{
		if (ResourceLocation.isValidResourceLocation(entityTypeId))
			this.register(new ResourceLocation(entityTypeId), lightFunction);
		else
			LucentRegistry.logInvalidResourceLocation(entityTypeId);
	}

	public void remove(EntityType<?> entityType)
	{
		if (PROVIDER_REGISTRY.remove(entityType) != null && LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> Removed [" + BuiltInRegistries.ENTITY_TYPE.getKey(entityType) + "]");
	}

	public List<EntityType<?>> getMatching(Predicate<EntityType<?>> itemPredicate)
	{
		return BuiltInRegistries.ENTITY_TYPE.stream().filter(itemPredicate).toList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<EntityType<?>> getMatchingByName(Predicate<ResourceLocation> namePredicate)
	{
		return (List) BuiltInRegistries.ENTITY_TYPE.keySet().stream().filter(namePredicate).map(BuiltInRegistries.ENTITY_TYPE::get).toList();
	}

	/**
	 * Gets the light of the passed entity
	 * 
	 * @param entity
	 * @return The light level of the entity. 0 by default
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static int get(Entity entity)
	{
		var lightings = PROVIDER_REGISTRY.getOrDefault(entity.getType(), DEFAULT_PROVIDERS);
		if (lightings.isEmpty())
			return 0;
		int ret = 0;
		for (EntityLighting lighting : lightings)
		{
			if (!lighting.worksUnderwater() && entity.level().getFluidState(entity.blockPosition()).is(FluidTags.WATER))
				continue;
			if (!lighting.worksOnLand() && !entity.level().getFluidState(entity.blockPosition()).is(FluidTags.WATER))
				continue;
			LightLevelProvider prov = lighting.provider();
			if (prov.getSource().isValidClass(entity))
			{
				int l = prov.getLightLevel(entity.level().registryAccess(), entity);
				if (l >= 15)
					return l;
				if (l > ret)
					ret = l;
			}
		}

		return ret;
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clear()
	{
		PROVIDER_REGISTRY.clear();
		DEFAULT_PROVIDERS.clear();
		clearCache();
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clearCache()
	{
		registeredBy.clear();
	}
}
