package com.legacy.lucent.api.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Holds the light source position getters of entities for dynamic lighting.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class EntityLightSourcePosRegistry
{
	/**
	 * Add entity Y position getters through
	 * {@link ILucentPlugin#registerEntityLightSourcePositionGetter(EntityLightSourcePosRegistry)}
	 * on config load or when loading into a world
	 * 
	 * @see #get(Entity)
	 */
	public static final Map<EntityType<?>, Function<Entity, Vec3>> REGISTRY = new HashMap<>();

	private final BiConsumer<EntityType<?>, Function<Entity, Vec3>> register;

	public EntityLightSourcePosRegistry(BiConsumer<EntityType<?>, Function<Entity, Vec3>> register)
	{
		this.register = register;
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> void register(EntityType<T> entityType, Function<T, Vec3> positionGetter)
	{
		this.register.accept(entityType, (Function<Entity, Vec3>) positionGetter);
	}

	@SuppressWarnings("unchecked")
	public void register(ResourceLocation entityTypeId, Function<Entity, Vec3> positionGetter)
	{
		if (BuiltInRegistries.ENTITY_TYPE.containsKey(entityTypeId))
			this.register((EntityType<Entity>) BuiltInRegistries.ENTITY_TYPE.get(entityTypeId), positionGetter);
		else if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + entityTypeId + "] was not found in registry");
	}

	public void register(String entityTypeId, Function<Entity, Vec3> positionGetter)
	{
		this.register(new ResourceLocation(entityTypeId), positionGetter);
	}

	/**
	 * Gets the position at the middle of the entity based on it's bounding box or
	 * the registered override
	 * 
	 * @param entity
	 * @return The entity's center position, or the registered override
	 */
	public static Vec3 get(Entity entity)
	{
		Function<Entity, Vec3> getter = REGISTRY.get(entity.getType());
		if (getter != null)
			return getter.apply(entity);
		return new Vec3(entity.getX(), entity.getBbHeight() / 2.0 + entity.getY(), entity.getZ());
	}
}
