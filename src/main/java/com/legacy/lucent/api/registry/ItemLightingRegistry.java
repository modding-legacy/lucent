package com.legacy.lucent.api.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.lucent.api.EntityBrightness;
import com.legacy.lucent.api.data.objects.ItemLighting;
import com.legacy.lucent.api.data.objects.LightLevelProvider;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Holds the light levels of items for dynamic lighting.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public class ItemLightingRegistry
{
	/**
	 * Add items through
	 * {@link ILucentPlugin#registerItemLightings(ItemLightingRegistry)} on config
	 * load or when loading into a world
	 * 
	 * @see #get(ItemStack)
	 * @see #get(Item)
	 */
	private static final Map<Item, List<ItemLighting>> PROVIDER_REGISTRY = new HashMap<>();
	private static final List<ItemLighting> DEFAULT_PROVIDERS = new ArrayList<>();
	private static Map<Item, LucentRegistry.Source> registeredBy = new HashMap<>();

	public ItemLightingRegistry()
	{
	}

	public void register(ItemLighting lighting)
	{
		LightLevelProvider<?> prov = lighting.provider();
		lighting.item().ifPresentOrElse(item ->
		{
			if (prov instanceof LightLevelProvider.Direct<?> direct && direct.value() <= -1)
			{
				this.remove(item);
			}
			else
			{
				boolean overwrote = false;
				var source = registeredBy.get(item);
				if (source != null && source.isBuiltin && source.priority < LucentRegistry.currentSource.priority)
				{
					List<ItemLighting> list = new ArrayList<>();
					list.add(lighting);
					PROVIDER_REGISTRY.put(item, list);
					overwrote = true;
				}
				else
				{
					PROVIDER_REGISTRY.computeIfAbsent(item, i -> new ArrayList<>()).add(lighting);
				}
				registeredBy.put(item, LucentRegistry.currentSource);
				if (LucentConfig.CLIENT.logRegistry.get())
				{
					LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [" + BuiltInRegistries.ITEM.getKey(item) + "] " + (overwrote ? "overwrote " : "added ") + prov);
				}
			}
		}, () ->
		{
			DEFAULT_PROVIDERS.add(lighting);
			if (LucentConfig.CLIENT.logRegistry.get())
			{
				LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [all items] added " + prov);
			}
		});
	}

	public void register(ItemLighting.Builder itemLightingBuilder)
	{
		if (!itemLightingBuilder.hasFailed())
			this.register(itemLightingBuilder.build());
	}

	public void register(@Nullable Item item, LightLevelProvider<?> lightLevelProvider)
	{
		this.register(ItemLighting.builder().item(item).light(lightLevelProvider));
	}

	public void register(LightLevelProvider<?> lightLevelProvider)
	{
		this.register(null, lightLevelProvider);
	}

	public void register(Item item, int lightLevel)
	{
		if (lightLevel <= -1)
			this.remove(item);
		else
			this.register(ItemLighting.builder().item(item).light(new LightLevelProvider.Direct<>(lightLevel)));
	}

	public void register(ResourceLocation itemId, int lightLevel)
	{
		if (BuiltInRegistries.ITEM.containsKey(itemId))
			this.register(BuiltInRegistries.ITEM.get(itemId), lightLevel);
		else if (LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + itemId + "] was not found in registry");
	}

	public void register(String itemId, int lightLevel)
	{
		if (ResourceLocation.isValidResourceLocation(itemId))
			this.register(new ResourceLocation(itemId), lightLevel);
		else
			LucentRegistry.logInvalidResourceLocation(itemId);
	}

	public void remove(Item item)
	{
		if (PROVIDER_REGISTRY.remove(item) != null && LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> Removed [" + BuiltInRegistries.ITEM.getKey(item) + "]");
	}

	public List<Item> getMatching(Predicate<Item> itemPredicate)
	{
		return BuiltInRegistries.ITEM.stream().filter(itemPredicate).toList();
	}

	public List<Item> getMatchingByName(Predicate<ResourceLocation> namePredicate)
	{
		return BuiltInRegistries.ITEM.keySet().stream().filter(namePredicate).map(BuiltInRegistries.ITEM::get).toList();
	}

	/**
	 * Gets the light of the passed item
	 * 
	 * @param item
	 * @return The light level of the item. 0 by default
	 */
	@Deprecated(forRemoval = true, since = "1.19")
	public static int get(Item item)
	{
		return get(item.getDefaultInstance());
	}

	/**
	 * Gets the light of the passed item stack
	 * 
	 * @param itemStack
	 * @return The light level of the item. 0 by default
	 */
	public static int get(ItemStack itemStack)
	{
		return get(itemStack, EntityBrightness.activeEntity);
	}

	/**
	 * Gets the light of the passed item stack
	 * 
	 * @param itemStack
	 * @param entity
	 *            The entity that owns the item in question
	 * @return The light level of the item. 0 by default
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static int get(ItemStack itemStack, Entity entity)
	{
		if (itemStack == null || itemStack.isEmpty())
			return 0;
		var lightings = PROVIDER_REGISTRY.getOrDefault(itemStack.getItem(), DEFAULT_PROVIDERS);
		if (lightings.isEmpty())
			return 0;
		int ret = 0;

		for (ItemLighting lighting : lightings)
		{
			if (!lighting.worksUnderwater() && entity.level().getFluidState(entity.blockPosition()).is(FluidTags.WATER))
				continue;
			if (!lighting.worksOnLand() && !entity.level().getFluidState(entity.blockPosition()).is(FluidTags.WATER))
				continue;
			LightLevelProvider prov = lighting.provider();
			if (prov.getSource().isValidClass(itemStack))
			{
				int l = prov.getLightLevel(entity.level().registryAccess(), itemStack);
				if (l >= 15)
					return l;
				if (l > ret)
					ret = l;
			}
		}
		return ret;
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clear()
	{
		PROVIDER_REGISTRY.clear();
		DEFAULT_PROVIDERS.clear();
		clearCache();
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clearCache()
	{
		registeredBy.clear();
	}
}
