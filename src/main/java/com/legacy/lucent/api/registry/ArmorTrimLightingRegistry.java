package com.legacy.lucent.api.registry;

import java.util.HashMap;
import java.util.Map;

import com.legacy.lucent.api.data.objects.EmissiveArmorTrimTexture;
import com.legacy.lucent.api.plugin.ILucentPlugin;
import com.legacy.lucent.core.LucentConfig;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.armortrim.ArmorTrim;
import net.minecraft.world.item.armortrim.TrimMaterial;

/**
 * Holds the light level of armor trim textures for emissive rendering.
 * 
 * @author Silver_David
 *
 */
public class ArmorTrimLightingRegistry
{
	/**
	 * Add textures through
	 * {@link ILucentPlugin#registerArmorTrimTextureLightings(ArmorTrimLightingRegistry)}
	 * on config load or when loading into a world.
	 * 
	 * @see #get(ResourceKey)
	 */
	private static final Map<ResourceKey<TrimMaterial>, Integer> REGISTRY = new HashMap<>();

	public ArmorTrimLightingRegistry()
	{
	}

	public void register(EmissiveArmorTrimTexture emissiveTexture)
	{
		ResourceKey<TrimMaterial> trimMaterial = emissiveTexture.trimMaterial();
		int light = emissiveTexture.light();
		if (light <= -1)
		{
			this.remove(trimMaterial);
		}
		else
		{
			REGISTRY.put(trimMaterial, light);
			if (LucentConfig.CLIENT.logRegistry.get())
			{
				LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> [" + trimMaterial.location() + "] set to " + light);
			}
		}
	}

	public void register(ResourceKey<TrimMaterial> trimMaterial, int lightLevel)
	{
		this.register(new EmissiveArmorTrimTexture(trimMaterial, lightLevel));
	}

	public void remove(ResourceKey<TrimMaterial> trimMaterial)
	{
		if (REGISTRY.remove(trimMaterial) != null && LucentConfig.CLIENT.logRegistry.get())
			LucentMod.LOGGER.info("  [" + LucentRegistry.sourceName + "] -> Removed [" + trimMaterial.location() + "]");
	}

	/**
	 * Gets the light of the passed texture file location
	 * 
	 * @param trimMaterial
	 * @return The light level of the trim material. 0 by default
	 */
	public static int get(ResourceKey<TrimMaterial> trimMaterial)
	{
		Integer light = REGISTRY.get(trimMaterial);
		return light != null ? light : 0;
	}
	
	public static int get(ArmorTrim armorTrim)
	{
		return armorTrim.material().unwrapKey().map(ArmorTrimLightingRegistry::get).orElse(0);
	}

	/**
	 * DO NOT CALL THIS. INTERNAL ONLY.
	 */
	@Deprecated
	public static void clear()
	{
		REGISTRY.clear();
	}
}
