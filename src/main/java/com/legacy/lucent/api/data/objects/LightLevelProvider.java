package com.legacy.lucent.api.data.objects;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.logging.log4j.util.Strings;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.ResourceLocationException;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.commands.arguments.NbtPathArgument;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.nbt.NumericTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.nbt.TagParser;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.armortrim.ArmorTrim;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;

public interface LightLevelProvider<T>
{
	public static final byte MIN_LIGHT = 0, MAX_LIGHT = 15;

	public static Codec<LightLevelProvider<?>> codec(Source source)
	{
		Codec<LightLevelProvider<?>> codec = LightLevelProviderType.REGISTRY_CODEC.dispatch(LightLevelProvider::getType, LightLevelProviderType::getCodec);
		if (source != Source.ANY)
		{
			Function<LightLevelProvider<?>, DataResult<LightLevelProvider<?>>> validate = prov ->
			{
				if (prov.getSource().children.contains(source))
				{
					return DataResult.success(prov);
				}
				return DataResult.error(() -> LightLevelProviderType.REGISTRY.inverse().get(prov.getType()) + " is not applicable on " + source);
			};

			codec = codec.flatXmap(validate, validate);
		}

		Codec<LightLevelProvider<?>> tryDirect = Codec.either(Codec.INT, codec).xmap(either -> either.map(Direct::new, Function.identity()), prov -> Either.right(prov));

		return tryDirect;
	}

	int getLightLevel(RegistryAccess registryAccess, T o);

	LightLevelProviderType<? extends LightLevelProvider<?>> getType();

	Source getSource();

	public static Optional<NbtPathArgument.NbtPath> parseNbtPath(String tagPath)
	{
		try
		{
			return Optional.of(new NbtPathArgument().parse(new StringReader(tagPath)));
		}
		catch (CommandSyntaxException e)
		{
			return Optional.empty();
		}
	}

	public static Optional<CompoundTag> parseTag(String tagString)
	{
		try
		{
			tagString = tagString.replace("'", "\"");
			return Optional.of(new TagParser(new StringReader(tagString)).readStruct());
		}
		catch (CommandSyntaxException e)
		{
			return Optional.empty();
		}
	}

	public static enum Source
	{
		ITEM_STACK("item", ItemStack.class),
		ENTITY("entity", Entity.class),
		ANY("any", Object.class, Source.ITEM_STACK, Source.ENTITY);

		private final Class<?> validator;
		private final Set<Source> children;
		private final String name;

		Source(String name, Class<?> validator, Source... children)
		{
			this.name = name;
			this.validator = validator;
			Set<Source> s = new HashSet<>();
			s.add(this);
			for (var child : children)
				s.add(child);
			this.children = Set.copyOf(s);
		}

		public boolean isValidClass(Object o)
		{
			return this.validator.isInstance(o);
		}

		@Override
		public String toString()
		{
			return this.name;
		}
	}

	public static interface LightLevelProviderType<A extends LightLevelProvider<?>>
	{
		static final BiMap<ResourceLocation, LightLevelProviderType<?>> REGISTRY = HashBiMap.create();

		static final Codec<LightLevelProviderType<?>> REGISTRY_CODEC = ResourceLocation.CODEC.flatXmap(key ->
		{
			return Optional.ofNullable(REGISTRY.get(key)).map(DataResult::success).orElseGet(() -> DataResult.error(() -> "Unknown light level provider type for key " + key + ". Registered providers: " + REGISTRY.keySet()));
		}, type ->
		{
			return Optional.ofNullable(REGISTRY.inverse().get(type)).map(DataResult::success).orElseGet(() -> DataResult.error(() -> "Unknown light level provider type for object " + type));
		});

		LightLevelProviderType<Direct<?>> DIRECT = register("direct", () -> Direct.CODEC);
		LightLevelProviderType<MatchBlock<?>> MATCH_BLOCK = register("match_block", () -> MatchBlock.CODEC);
		LightLevelProviderType<IfNbt<?>> IF_NBT = register("if_nbt", () -> IfNbt.CODEC);
		LightLevelProviderType<MatchNbt<?>> MATCH_NBT = register("match_nbt", () -> MatchNbt.CODEC);
		LightLevelProviderType<MatchNbtItem<?>> MATCH_NBT_ITEM = register("match_nbt_item", () -> MatchNbtItem.CODEC);
		LightLevelProviderType<MatchNbtBlock<?>> MATCH_NBT_BLOCK = register("match_nbt_block", () -> MatchNbtBlock.CODEC);
		LightLevelProviderType<MatchNbtFluid<?>> MATCH_NBT_FLUID = register("match_nbt_fluid", () -> MatchNbtFluid.CODEC);
		LightLevelProviderType<MatchEnchantment> MATCH_ENCHANTMENT = register("match_enchantment", () -> MatchEnchantment.CODEC);
		LightLevelProviderType<MatchArmorTrim> MATCH_ARMOR_TRIM = register("match_armor_trim", () -> MatchArmorTrim.CODEC);
		LightLevelProviderType<Composite<?>> COMPOSITE = register("composite", () -> Composite.CODEC);
		LightLevelProviderType<MinComposite<?>> MIN_COMPOSITE = register("min_composite", () -> MinComposite.CODEC);
		LightLevelProviderType<AverageComposite<?>> AVERAGE_COMPOSITE = register("average_composite", () -> AverageComposite.CODEC);

		/**
		 * Not used by resource packs. Empty codec.
		 */
		LightLevelProviderType<Custom<?>> CUSTOM = register("custom", () -> Custom.CODEC);

		Codec<A> getCodec();

		private static <A extends LightLevelProvider<?>> LightLevelProviderType<A> register(String name, LightLevelProviderType<A> type)
		{
			return register(LucentMod.locate(name), type);
		}

		public static <A extends LightLevelProvider<?>> LightLevelProviderType<A> register(ResourceLocation name, LightLevelProviderType<A> type)
		{
			REGISTRY.put(name, type);
			return type;
		}

		default String getTypeName()
		{
			return Optional.ofNullable(REGISTRY.inverse().get(this)).map(ResourceLocation::toString).orElse("null");
		}
	}

	public static record Direct<T>(byte value) implements LightLevelProvider<T>
	{
		private static final Codec<Direct<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.BYTE.fieldOf("value").forGetter(r ->
			{
				return r.value;
			})).apply(instance, Direct::new);
		});

		public static final Direct<?> MAX_LIGHTING = new Direct<>(MAX_LIGHT);

		public Direct(int value)
		{
			this((byte) Mth.clamp(value, MIN_LIGHT, MAX_LIGHT));
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			return this.value;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.DIRECT;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[value=%d]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.value);
		}
	}

	public static class MatchBlock<T> implements LightLevelProvider<T>
	{
		private static final Codec<MatchBlock<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(BuiltInRegistries.BLOCK.byNameCodec().fieldOf("block").forGetter(r ->
			{
				return r.block;
			})).apply(instance, MatchBlock::new);
		});

		private final Block block;
		private byte cachedVal = Byte.MIN_VALUE;

		public MatchBlock(Block block)
		{
			this.block = block;
		}

		@SuppressWarnings("deprecation")
		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			if (this.cachedVal == Byte.MIN_VALUE)
			{
				Minecraft mc = Minecraft.getInstance();
				BlockState state = this.block.defaultBlockState();
				this.cachedVal = (byte) Mth.clamp(mc.level == null ? state.getLightEmission() : state.getLightEmission(mc.level, LucentRegistry.DEFAULT_POS), MIN_LIGHT, MAX_LIGHT);
			}
			return this.cachedVal;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_BLOCK;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[block=%s]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), BuiltInRegistries.BLOCK.getKey(this.block));
		}
	}

	public static record IfNbt<T>(CompoundTag tag, int value) implements LightLevelProvider<T>, TagGetter
	{

		private static final Codec<IfNbt<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.STRING.fieldOf("tag").flatXmap(s -> parseTag(s).map(DataResult::success).orElse(DataResult.error(() -> s + " is not a valid nbt tag.")), nbt -> DataResult.success(nbt.getAsString())).forGetter(r ->
			{
				return r.tag;
			}), Codec.INT.fieldOf("value").forGetter(r ->
			{
				return r.value;
			})).apply(instance, IfNbt::new);
		});

		public IfNbt(String tagString, int value)
		{
			this(parseTag(tagString).orElseThrow(() -> new IllegalArgumentException("[lucent] [light provider = " + LightLevelProviderType.IF_NBT.getTypeName() + "] " + tagString + " could not be read as an nbt tag.")), value);
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			Optional<CompoundTag> opNbt = this.getNbt(o);
			if (opNbt.isPresent() && NbtUtils.compareNbt(this.tag, opNbt.get(), true))
				return this.value;
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.IF_NBT;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[tag=%s, value=%d]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.tag, this.value);
		}
	}

	public static record MatchNbt<T>(NbtPathArgument.NbtPath tagPath, float scale) implements LightLevelProvider<T>, TagGetter
	{

		private static final Codec<MatchNbt<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.STRING.fieldOf("tag_path").flatXmap(s -> parseNbtPath(s).map(DataResult::success).orElse(DataResult.error(() -> s + " is not a valid nbt tag path.")), t -> DataResult.success(t.toString())).forGetter(r ->
			{
				return r.tagPath;
			}), Codec.FLOAT.fieldOf("scale").forGetter(r ->
			{
				return r.scale;
			})).apply(instance, MatchNbt::new);
		});

		public MatchNbt(String tagPath, float scale)
		{
			this(parseNbtPath(tagPath).orElseThrow(() -> new IllegalArgumentException("[lucent] [light provider = " + LightLevelProviderType.MATCH_NBT.getTypeName() + "] " + tagPath + " could not be read as an nbt tag path.")), scale);
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			var opNbt = this.getNbt(o);
			if (opNbt.isPresent())
			{
				Optional<Tag> opTag = this.getTag(opNbt.get(), this.tagPath);
				if (opTag.isPresent())
				{
					if (opTag.get() instanceof NumericTag numeric)
						return Mth.floor(numeric.getAsDouble() * this.scale);
					else
						return Mth.floor(this.scale);
				}
			}
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_NBT;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[tag_path=%s, scale=%f]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.tagPath, this.scale);
		}
	}

	public static record MatchNbtItem<T>(NbtPathArgument.NbtPath tagPath) implements LightLevelProvider<T>, TagGetter
	{

		private static final Codec<MatchNbtItem<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.STRING.fieldOf("tag_path").flatXmap(s -> parseNbtPath(s).map(DataResult::success).orElse(DataResult.error(() -> s + " is not a valid nbt tag path.")), t -> DataResult.success(t.toString())).forGetter(r ->
			{
				return r.tagPath;
			})).apply(instance, MatchNbtItem::new);
		});

		public MatchNbtItem(String tagPath)
		{
			this(parseNbtPath(tagPath).orElseThrow(() -> new IllegalArgumentException("[lucent] [light provider = " + LightLevelProviderType.MATCH_NBT_ITEM.getTypeName() + "] " + tagPath + " could not be read as an nbt tag path.")));
		}

		private static final Function<String, Item> ITEM_CACHE = Util.memoize(itemName ->
		{
			try
			{
				Item item = BuiltInRegistries.ITEM.get(new ResourceLocation(itemName));
				if (item != null)
					return item;
			}
			catch (ResourceLocationException e)
			{
				LucentMod.LOGGER.error(e);
				e.printStackTrace();
			}
			return Items.AIR;
		});

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			var opNbt = this.getNbt(o);
			if (opNbt.isPresent())
			{
				Optional<Tag> opTag = this.getTag(opNbt.get(), this.tagPath);
				if (opTag.isPresent())
				{
					Tag tag = opTag.get();
					ItemStack stack;
					if (tag instanceof CompoundTag stackTag)
					{
						stack = ItemStack.of(stackTag);
					}
					else if (tag instanceof StringTag stringTag)
					{
						stack = ITEM_CACHE.apply(stringTag.getAsString()).getDefaultInstance();
					}
					else
					{
						stack = ItemStack.EMPTY;
					}
					return ItemLightingRegistry.get(stack);
				}
			}
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_NBT_ITEM;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[tag_path=%s]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.tagPath);
		}
	}

	public static record MatchNbtBlock<T>(NbtPathArgument.NbtPath tagPath) implements LightLevelProvider<T>, TagGetter
	{

		private static final Codec<MatchNbtBlock<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.STRING.fieldOf("tag_path").flatXmap(s -> parseNbtPath(s).map(DataResult::success).orElse(DataResult.error(() -> s + " is not a valid nbt tag path.")), t -> DataResult.success(t.toString())).forGetter(r ->
			{
				return r.tagPath;
			})).apply(instance, MatchNbtBlock::new);
		});

		@SuppressWarnings("deprecation")
		private static final Function<String, Integer> BLOCK_LIGHT_CACHE = Util.memoize(blockName ->
		{
			try
			{
				Block block = BuiltInRegistries.BLOCK.get(new ResourceLocation(blockName));
				if (block != null)
					return block.defaultBlockState().getLightEmission();
			}
			catch (ResourceLocationException e)
			{
				LucentMod.LOGGER.error(e);
				e.printStackTrace();
			}
			return 0;
		});

		public MatchNbtBlock(String tagPath)
		{
			this(parseNbtPath(tagPath).orElseThrow(() -> new IllegalArgumentException("[lucent] [light provider = " + LightLevelProviderType.MATCH_NBT_BLOCK.getTypeName() + "] " + tagPath + " could not be read as an nbt tag path.")));
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			var opNbt = this.getNbt(o);
			if (opNbt.isPresent())
			{
				Optional<Tag> opTag = this.getTag(opNbt.get(), this.tagPath);
				if (opTag.isPresent())
				{
					return BLOCK_LIGHT_CACHE.apply(opTag.get().getAsString());
				}
			}
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_NBT_BLOCK;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[tag_path=%s]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.tagPath);
		}
	}

	public static record MatchNbtFluid<T>(NbtPathArgument.NbtPath tagPath) implements LightLevelProvider<T>, TagGetter
	{

		private static final Codec<MatchNbtFluid<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(Codec.STRING.fieldOf("tag_path").flatXmap(s -> parseNbtPath(s).map(DataResult::success).orElse(DataResult.error(() -> s + " is not a valid nbt tag path.")), t -> DataResult.success(t.toString())).forGetter(r ->
			{
				return r.tagPath;
			})).apply(instance, MatchNbtFluid::new);
		});

		@SuppressWarnings("deprecation")
		private static final Function<String, Integer> FLUID_LIGHT_CACHE = Util.memoize(fluidName ->
		{
			try
			{
				Fluid fluid = BuiltInRegistries.FLUID.get(new ResourceLocation(fluidName));
				if (fluid != null)
					return fluid.defaultFluidState().createLegacyBlock().getLightEmission();
			}
			catch (ResourceLocationException e)
			{
				LucentMod.LOGGER.error(e);
				e.printStackTrace();
			}
			return 0;
		});

		public MatchNbtFluid(String tagPath)
		{
			this(parseNbtPath(tagPath).orElseThrow(() -> new IllegalArgumentException("[lucent] [light provider = " + LightLevelProviderType.MATCH_NBT_FLUID.getTypeName() + "] " + tagPath + " could not be read as an nbt tag path.")));
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			var opNbt = this.getNbt(o);
			if (opNbt.isPresent())
			{
				Optional<Tag> opTag = this.getTag(opNbt.get(), this.tagPath);
				if (opTag.isPresent())
				{
					return FLUID_LIGHT_CACHE.apply(opTag.get().getAsString());
				}
			}
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_NBT_FLUID;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[tag_path=%s]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.tagPath);
		}
	}

	public static record MatchEnchantment(Enchantment enchantment, float scale) implements LightLevelProvider<ItemStack>
	{

		private static final Codec<MatchEnchantment> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(BuiltInRegistries.ENCHANTMENT.byNameCodec().fieldOf("enchantment").forGetter(r ->
			{
				return r.enchantment;
			}), Codec.FLOAT.fieldOf("scale").forGetter(r ->
			{
				return r.scale;
			})).apply(instance, MatchEnchantment::new);
		});

		@Override
		public int getLightLevel(RegistryAccess registryAccess, ItemStack o)
		{
			Integer level = o.getAllEnchantments().get(this.enchantment);
			return level != null ? Mth.floor((o.getEnchantmentLevel(this.enchantment) + 1) * this.scale) : 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_ENCHANTMENT;
		}

		@Override
		public Source getSource()
		{
			return Source.ITEM_STACK;
		}

		@Override
		public String toString()
		{
			return String.format("%s[enchantment=%s, scale=%f]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), BuiltInRegistries.ENCHANTMENT.getKey(this.enchantment), this.scale);
		}
	}

	public static record MatchArmorTrim(ResourceKey<TrimMaterial> trimMaterial, int value) implements LightLevelProvider<ItemStack>
	{

		private static final Codec<MatchArmorTrim> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(ResourceKey.codec(Registries.TRIM_MATERIAL).fieldOf("trim_material").forGetter(r ->
			{
				return r.trimMaterial;
			}), Codec.INT.fieldOf("value").forGetter(r ->
			{
				return r.value;
			})).apply(instance, MatchArmorTrim::new);
		});

		@Override
		public int getLightLevel(RegistryAccess registryAccess, ItemStack o)
		{
			Optional<ArmorTrim> trim = ArmorTrim.getTrim(registryAccess, o, false);
			if (trim.isPresent() && trim.get().material().is(this.trimMaterial))
				return this.value;
			return 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MATCH_ARMOR_TRIM;
		}

		@Override
		public Source getSource()
		{
			return Source.ITEM_STACK;
		}

		@Override
		public String toString()
		{
			return String.format("%s[armor_trim_material=%s, value=%f]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), this.trimMaterial.location(), this.value);
		}
	}

	public static class Composite<T> implements LightLevelProvider<T>
	{
		private static final Codec<Composite<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(LightLevelProvider.codec(Source.ANY).listOf().fieldOf("children").forGetter(r ->
			{
				return r.children;
			})).apply(instance, Composite::new);
		});

		protected final List<LightLevelProvider<?>> children;

		public Composite(List<LightLevelProvider<?>> children)
		{
			this.children = children;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			OptionalInt optionalRet = this.children.stream().mapToInt(lp ->
			{
				if (lp.getSource().isValidClass(o))
					return ((LightLevelProvider) lp).getLightLevel(registryAccess, o);
				return 0;
			}).filter(i -> i > 0).max();
			return optionalRet.isPresent() ? optionalRet.getAsInt() : 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.COMPOSITE;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s[%s]", LightLevelProviderType.REGISTRY.inverse().get(this.getType()), Strings.join(this.children, ','));
		}
	}

	public static class MinComposite<T> extends Composite<T>
	{
		private static final Codec<MinComposite<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(LightLevelProvider.codec(Source.ANY).listOf().fieldOf("children").forGetter(r ->
			{
				return r.children;
			})).apply(instance, MinComposite::new);
		});

		public MinComposite(List<LightLevelProvider<?>> children)
		{
			super(children);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			OptionalInt optionalRet = this.children.stream().mapToInt(lp ->
			{
				if (lp.getSource().isValidClass(o))
					return ((LightLevelProvider) lp).getLightLevel(registryAccess, o);
				return 0;
			}).filter(i -> i > 0).min();
			return optionalRet.isPresent() ? optionalRet.getAsInt() : 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.MIN_COMPOSITE;
		}
	}

	public static class AverageComposite<T> extends Composite<T>
	{
		private static final Codec<AverageComposite<?>> CODEC = RecordCodecBuilder.create(instance ->
		{
			return instance.group(LightLevelProvider.codec(Source.ANY).listOf().fieldOf("children").forGetter(r ->
			{
				return r.children;
			})).apply(instance, AverageComposite::new);
		});

		public AverageComposite(List<LightLevelProvider<?>> children)
		{
			super(children);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			OptionalDouble optionalRet = this.children.stream().mapToInt(lp ->
			{
				if (lp.getSource().isValidClass(o))
					return ((LightLevelProvider) lp).getLightLevel(registryAccess, o);
				return 0;
			}).filter(i -> i > 0).average();
			return optionalRet.isPresent() ? (int) Math.round(optionalRet.getAsDouble()) : 0;
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.AVERAGE_COMPOSITE;
		}
	}

	public static record Custom<T>(BiFunction<RegistryAccess, T, Integer> lightFunction) implements LightLevelProvider<T>
	{
		private static final Custom<?> INSTANCE = new Custom<>(o -> 0);
		private static final Codec<Custom<?>> CODEC = new Codec<>()
		{
			public <U extends Object> DataResult<Pair<Custom<?>, U>> decode(DynamicOps<U> ops, U input)
			{
				return DataResult.error(() -> "Cannot decode light provider lucent:custom. This is to be used internally.", Pair.of(INSTANCE, input));
			}

			public <U extends Object> DataResult<U> encode(Custom<?> input, DynamicOps<U> ops, U prefix)
			{
				return DataResult.error(() -> "Cannot encode light provider lucent:custom. This is to be used internally.");
			}
		};

		public Custom(Function<T, Integer> lightFunction)
		{
			this((r, o) -> lightFunction.apply(o));
		}

		@Override
		public int getLightLevel(RegistryAccess registryAccess, T o)
		{
			return this.lightFunction.apply(registryAccess, o);
		}

		@Override
		public LightLevelProviderType<? extends LightLevelProvider<?>> getType()
		{
			return LightLevelProviderType.CUSTOM;
		}

		@Override
		public Source getSource()
		{
			return Source.ANY;
		}

		@Override
		public String toString()
		{
			return String.format("%s", LightLevelProviderType.REGISTRY.inverse().get(this.getType()));
		}
	}

	public static interface TagGetter
	{
		default Optional<CompoundTag> getNbt(Object o)
		{
			if (o instanceof ItemStack)
			{
				return Optional.ofNullable(((ItemStack) o).getTag());
			}
			else if (o instanceof Entity)
			{
				var t = new CompoundTag();
				((Entity) o).saveWithoutId(t);
				return Optional.of(t);
			}
			else if (o instanceof BlockEntity)
			{
				return Optional.of(((BlockEntity) o).saveWithoutMetadata());
			}
			return Optional.empty();
		}

		default Optional<Tag> getTag(CompoundTag tag, NbtPathArgument.NbtPath tagPath)
		{
			try
			{
				Collection<Tag> tags = tagPath.get(tag);
				Iterator<Tag> it = tags.iterator();
				Tag ret = it.next();
				if (it.hasNext())
					LucentMod.LOGGER.error("Found multiple tags matching {}", tagPath);
				return Optional.ofNullable(ret);
			}
			catch (CommandSyntaxException e)
			{
				return Optional.empty();
			}
		}
	}
}
