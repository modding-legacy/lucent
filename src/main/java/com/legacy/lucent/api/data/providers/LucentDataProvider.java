package com.legacy.lucent.api.data.providers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonObject;
import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.data.managers.DatapackType;
import com.legacy.lucent.core.data.managers.LucentDataManager;
import com.mojang.serialization.JsonOps;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;

public abstract class LucentDataProvider<K, V> implements DataProvider
{
	protected static final Logger LOGGER = LogManager.getLogger();
	private final BiMap<ResourceLocation, V> registry = HashBiMap.create();
	protected final PackOutput packOutput;
	protected final String directory;
	protected final DatapackType<K, V> type;
	protected final LucentDataManager<K, V> manager;
	private CompletableFuture<HolderLookup.Provider> lookupProvider;

	public LucentDataProvider(LucentDataManager<K, V> manager, PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		this.packOutput = output;
		this.type = manager.getType();
		this.directory = manager.getDirectory();
		this.manager = manager;
		this.lookupProvider = lookupProvider;
	}

	public net.minecraft.server.packs.PackType getPackType()
	{
		return net.minecraft.server.packs.PackType.SERVER_DATA;
	}

	public static abstract class Client<K, V> extends LucentDataProvider<K, V>
	{
		public Client(LucentDataManager<K, V> manager, PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider)
		{
			super(manager, output, lookupProvider);
		}

		@Override
		public net.minecraft.server.packs.PackType getPackType()
		{
			return net.minecraft.server.packs.PackType.CLIENT_RESOURCES;
		}
	}

	@Override
	public CompletableFuture<?> run(CachedOutput cache)
	{
		Path outputFolder = this.packOutput.getOutputFolder();
		try
		{
			var prov = this.lookupProvider.get();
			this.gatherData(prov);
		}
		catch (InterruptedException | ExecutionException e1)
		{
			e1.printStackTrace();
		}

		List<CompletableFuture<?>> list = new ArrayList<>();

		for (Map.Entry<ResourceLocation, V> dataEntry : this.getRegistry().entrySet())
		{
			Path outputFile = this.createPath(outputFolder, dataEntry.getKey());
			try
			{
				V val = dataEntry.getValue();
				JsonObject json = this.type.valueCodec().encodeStart(JsonOps.INSTANCE, val).getOrThrow(false, IllegalStateException::new).getAsJsonObject();
				list.add(DataProvider.saveStable(cache, json, outputFile));
			}
			catch (Exception e)
			{
				LOGGER.error("Couldn't save {} {}", this.directory, outputFile, e);
			}
		}

		Map<K, V> keyedData = this.registry.entrySet().stream().collect(Collectors.toMap(e -> this.type.keyCompute().apply(e.getKey(), e.getValue()), e -> e.getValue()));
		this.manager.injectData(keyedData);

		return CompletableFuture.allOf(list.toArray(CompletableFuture[]::new));
	}

	/**
	 * Register datapack data during this with
	 * {@link #register(ResourceLocation, Object)}
	 * 
	 * @param lookupProv
	 */
	protected abstract void gatherData(HolderLookup.Provider lookupProv);

	protected BiMap<ResourceLocation, V> getRegistry()
	{
		return this.registry;
	}

	/**
	 * Registers the data passed. Call this during
	 * {@link #gatherData(HolderLookup.Provider)}. The file name will be created by
	 * calling {@link #locate(String)} on {@code fileName}
	 * 
	 * @param fileName
	 * @param object
	 * @throws IllegalArgumentException
	 *             If a duplicate key is registered.
	 */
	protected void register(String fileName, V object) throws IllegalArgumentException
	{
		this.register(this.locate(fileName), object);
	}

	/**
	 * Registers the data passed. Call this during
	 * {@link #gatherData(HolderLookup.Provider)}.
	 * 
	 * @param fileLocation
	 * @param object
	 * @throws IllegalArgumentException
	 *             If a duplicate key is registered.
	 */
	protected void register(ResourceLocation fileLocation, V object) throws IllegalArgumentException
	{
		if (this.getRegistry().putIfAbsent(fileLocation, object) != null)
			throw new IllegalArgumentException("Duplicate key! Item " + this.directory + " exists under the name " + fileLocation);
	}

	protected Path createPath(Path outputFolder, ResourceLocation dataEntryName)
	{
		return outputFolder.resolve(this.getPackType().getDirectory() + "/" + dataEntryName.getNamespace() + "/" + this.directory + "/" + dataEntryName.getPath() + ".json");
	}

	@Override
	public String getName()
	{
		return this.directory;
	}

	/**
	 * @param resourceLocationPath
	 */
	public ResourceLocation locate(String resourceLocationPath)
	{
		return new ResourceLocation(LucentMod.MODID, resourceLocationPath);
	}
}
