package com.legacy.lucent.api.data.providers;

import java.util.concurrent.CompletableFuture;

import com.legacy.lucent.api.data.objects.EntityLighting;
import com.legacy.lucent.core.data.managers.LucentAssets;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;

public abstract class EntityLightingProvider extends LucentDataProvider.Client<ResourceLocation, EntityLighting<?>>
{
	public EntityLightingProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProv)
	{
		super(LucentAssets.ENTITY_LIGHTING, output, lookupProv);
	}

	/**
	 * Registers the entity lighting for the entity type passed.
	 * 
	 * @param entityType
	 *            Used to name the file only
	 * @param entityLighting
	 * @throws NullPointerException
	 *             If the entity type has no registry name.
	 * @throws IllegalArgumentException
	 *             If a duplicate key is registered.
	 */
	protected void register(EntityType<?> entityType, EntityLighting<?> entityLighting) throws NullPointerException, IllegalArgumentException
	{
		ResourceLocation entityID = BuiltInRegistries.ENTITY_TYPE.getKey(entityType);
		if (entityID == null)
			throw new NullPointerException("The item passed had a null registry name");
		this.register(entityID.getPath(), entityLighting);
	}
}
