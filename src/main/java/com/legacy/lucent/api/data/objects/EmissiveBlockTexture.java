package com.legacy.lucent.api.data.objects;

import com.legacy.lucent.core.LucentRegistry;
import com.legacy.lucent.core.data.managers.DatapackType;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.resources.ResourceLocation;

public record EmissiveBlockTexture(ResourceLocation texture, int light)
{

	private static final Codec<EmissiveBlockTexture> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ResourceLocation.CODEC.fieldOf("texture").forGetter(r ->
		{
			return r.texture;
		}), Codec.intRange(0, 15).fieldOf("light").forGetter(r ->
		{
			return r.light;
		})).apply(instance, EmissiveBlockTexture::new);
	});

	public static final DatapackType<ResourceLocation, EmissiveBlockTexture> TYPE = DatapackType.byName(EmissiveBlockTexture.class, "emissive_textures/block", CODEC);

	public EmissiveBlockTexture(ResourceLocation texture, int light)
	{
		this.texture = texture;
		this.light = LucentRegistry.clampLight(light);
	}
}
