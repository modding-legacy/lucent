package com.legacy.lucent.api.data.providers;

import java.util.concurrent.CompletableFuture;

import com.legacy.lucent.api.data.objects.EmissiveArmorTrimTexture;
import com.legacy.lucent.core.data.managers.LucentAssets;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.armortrim.TrimMaterial;

public abstract class EmissiveArmorTrimTextureProvider extends LucentDataProvider<ResourceLocation, EmissiveArmorTrimTexture>
{
	public EmissiveArmorTrimTextureProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProv)
	{
		super(LucentAssets.EMISSIVE_ARMOR_TRIM_TEXTURE, output, lookupProv);
	}

	/**
	 * Registers the trim material as being emissive with the data passed
	 * 
	 * @param fileName
	 * @param trimMaterial
	 * @param lightLevel
	 * @throws IllegalArgumentException
	 */
	protected void register(String fileName, ResourceKey<TrimMaterial> trimMaterial, int lightLevel) throws IllegalArgumentException
	{
		this.register(fileName, new EmissiveArmorTrimTexture(trimMaterial, lightLevel));
	}
}
