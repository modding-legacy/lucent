package com.legacy.lucent.api.data.objects;

import com.legacy.lucent.core.LucentRegistry;
import com.legacy.lucent.core.data.managers.DatapackType;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.armortrim.TrimMaterial;

public record EmissiveArmorTrimTexture(ResourceKey<TrimMaterial> trimMaterial, int light)
{

	private static final Codec<EmissiveArmorTrimTexture> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ResourceKey.codec(Registries.TRIM_MATERIAL).fieldOf("trim_material").forGetter(r ->
		{
			return r.trimMaterial;
		}), Codec.intRange(0, 15).fieldOf("light").forGetter(r ->
		{
			return r.light;
		})).apply(instance, EmissiveArmorTrimTexture::new);
	});

	public static final DatapackType<ResourceLocation, EmissiveArmorTrimTexture> TYPE = DatapackType.byName(EmissiveArmorTrimTexture.class, "emissive_textures/armor_trim", CODEC);

	public EmissiveArmorTrimTexture(ResourceKey<TrimMaterial> trimMaterial, int light)
	{
		this.trimMaterial = trimMaterial;
		this.light = LucentRegistry.clampLight(light);
	}
}
