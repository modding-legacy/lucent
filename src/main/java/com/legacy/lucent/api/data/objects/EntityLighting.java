package com.legacy.lucent.api.data.objects;

import java.util.Optional;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;
import com.legacy.lucent.core.data.managers.DatapackType;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.block.Block;

public class EntityLighting<T extends Entity>
{
	private static final Codec<EntityLighting<?>> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(BuiltInRegistries.ENTITY_TYPE.byNameCodec().optionalFieldOf("id").forGetter(r ->
		{
			return r.entity;
		}), LightLevelProvider.codec(LightLevelProvider.Source.ENTITY).fieldOf("light").forGetter(r ->
		{
			return r.provider;
		}), Codec.BOOL.optionalFieldOf("works_underwater", true).forGetter(r ->
		{
			return r.worksUnderwater;
		}), Codec.BOOL.optionalFieldOf("works_on_land", true).forGetter(r ->
		{
			return r.worksOnLand;
		})).apply(instance, EntityLighting::new);
	});

	public static final DatapackType<ResourceLocation, EntityLighting<?>> TYPE = DatapackType.byName(EntityLighting.class, "dynamic_lighting/entity", CODEC);

	private final Optional<EntityType<?>> entity;
	private final LightLevelProvider<?> provider;
	private final boolean worksUnderwater;
	private final boolean worksOnLand;

	private EntityLighting(Optional<EntityType<?>> entity, LightLevelProvider<?> provider, boolean worksUnderwater, boolean worksOnLand)
	{
		this.entity = entity;
		this.provider = provider;
		this.worksUnderwater = worksUnderwater;
		this.worksOnLand = worksOnLand;
	}

	public static Builder builder()
	{
		return new Builder();
	}

	public Optional<EntityType<?>> entity()
	{
		return entity;
	}

	public LightLevelProvider<?> provider()
	{
		return provider;
	}

	public boolean worksUnderwater()
	{
		return worksUnderwater;
	}

	public boolean worksOnLand()
	{
		return worksOnLand;
	}

	public static class Builder
	{
		Optional<EntityType<? extends Entity>> entityType = Optional.empty();
		LightLevelProvider<?> provider = LightLevelProvider.Direct.MAX_LIGHTING;
		boolean worksUnderwater = true;
		boolean worksOnLand = true;

		boolean failed = false;

		private Builder()
		{
		}

		public Builder entity(@Nullable EntityType<?> entityType)
		{
			this.entityType = Optional.ofNullable(entityType);
			return this;
		}

		public Builder entity(ResourceLocation entityID)
		{
			if (BuiltInRegistries.ENTITY_TYPE.containsKey(entityID))
				return this.entity(BuiltInRegistries.ENTITY_TYPE.get(entityID));
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + entityID + "] was not found in registry");
			this.failed = true;
			return this;
		}

		public Builder entity(String entityID)
		{
			if (ResourceLocation.isValidResourceLocation(entityID))
				return this.entity(new ResourceLocation(entityID));
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + entityID + "] is not a valid resource location");
			this.failed = true;
			return this;
		}

		public Builder light(LightLevelProvider<?> provider)
		{
			this.provider = provider;
			return this;
		}

		public Builder light(int light)
		{
			return this.light(new LightLevelProvider.Direct<>(light));
		}

		public Builder light(Block block)
		{
			return this.light(new LightLevelProvider.MatchBlock<>(block));
		}

		public Builder light(Function<? extends Entity, Integer> function)
		{
			return this.light(new LightLevelProvider.Custom<>(function));
		}

		public Builder worksUnderwater(boolean worksUnderwater)
		{
			this.worksUnderwater = worksUnderwater;
			return this;
		}

		public Builder worksOnLand(boolean worksOnLand)
		{
			this.worksOnLand = worksOnLand;
			return this;
		}

		public boolean hasFailed()
		{
			return this.failed;
		}

		public EntityLighting<?> build()
		{
			return new EntityLighting<Entity>(entityType, provider, worksUnderwater, worksOnLand);
		}
	}
}
