package com.legacy.lucent.api.data.objects;

import java.util.Optional;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.lucent.core.LucentMod;
import com.legacy.lucent.core.LucentRegistry;
import com.legacy.lucent.core.data.managers.DatapackType;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;

public class ItemLighting
{
	private static final Codec<ItemLighting> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(BuiltInRegistries.ITEM.byNameCodec().optionalFieldOf("id").forGetter(r ->
		{
			return r.item;
		}), LightLevelProvider.codec(LightLevelProvider.Source.ITEM_STACK).fieldOf("light").forGetter(r ->
		{
			return r.provider;
		}), Codec.BOOL.optionalFieldOf("works_underwater", true).forGetter(r ->
		{
			return r.worksUnderwater;
		}), Codec.BOOL.optionalFieldOf("works_on_land", true).forGetter(r ->
		{
			return r.worksOnLand;
		})).apply(instance, ItemLighting::new);
	});

	public static final DatapackType<ResourceLocation, ItemLighting> TYPE = DatapackType.byName(ItemLighting.class, "dynamic_lighting/item", CODEC);

	private final Optional<Item> item;
	private final LightLevelProvider<?> provider;
	private final boolean worksUnderwater;
	private final boolean worksOnLand;

	private ItemLighting(Optional<Item> item, LightLevelProvider<?> provider, boolean worksUnderwater, boolean worksOnLand)
	{
		this.item = item;
		this.provider = provider;
		this.worksUnderwater = worksUnderwater;
		this.worksOnLand = worksOnLand;
	}

	public static Builder builder()
	{
		return new Builder();
	}

	public Optional<Item> item()
	{
		return item;
	}

	public LightLevelProvider<?> provider()
	{
		return provider;
	}

	public boolean worksUnderwater()
	{
		return worksUnderwater;
	}

	public boolean worksOnLand()
	{
		return worksOnLand;
	}

	public static class Builder
	{
		Optional<Item> item = Optional.empty();
		LightLevelProvider<?> provider = LightLevelProvider.Direct.MAX_LIGHTING;
		boolean worksUnderwater = true;
		boolean worksOnLand = true;

		boolean failed = false;

		private Builder()
		{
		}

		public Builder item(@Nullable Item item)
		{
			this.item = Optional.ofNullable(item);
			return this;
		}

		public Builder item(ResourceLocation itemID)
		{
			if (BuiltInRegistries.ITEM.containsKey(itemID))
				return this.item(BuiltInRegistries.ITEM.get(itemID));
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + itemID + "] was not found in registry");
			this.failed = true;
			return this;
		}

		public Builder item(String itemID)
		{
			if (ResourceLocation.isValidResourceLocation(itemID))
				return this.item(new ResourceLocation(itemID));
			LucentMod.LOGGER.warn("  [" + LucentRegistry.sourceName + "] -> [" + itemID + "] is not a valid resource location");
			this.failed = true;
			return this;
		}

		public Builder light(LightLevelProvider<?> provider)
		{
			this.provider = provider;
			return this;
		}

		public Builder light(int light)
		{
			return this.light(new LightLevelProvider.Direct<>(light));
		}

		public Builder light(Block block)
		{
			return this.light(new LightLevelProvider.MatchBlock<>(block));
		}

		public Builder light(Function<ItemStack, Integer> function)
		{
			return this.light(new LightLevelProvider.Custom<>(function));
		}

		public Builder worksUnderwater(boolean worksUnderwater)
		{
			this.worksUnderwater = worksUnderwater;
			return this;
		}

		public Builder worksOnLand(boolean worksOnLand)
		{
			this.worksOnLand = worksOnLand;
			return this;
		}

		public boolean hasFailed()
		{
			return this.failed;
		}

		public ItemLighting build()
		{
			return new ItemLighting(item, provider, worksUnderwater, worksOnLand);
		}
	}
}