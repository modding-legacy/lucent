package com.legacy.lucent.api.data.providers;

import java.util.concurrent.CompletableFuture;

import com.legacy.lucent.api.data.objects.ItemLighting;
import com.legacy.lucent.core.data.managers.LucentAssets;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;

public abstract class ItemLightingProvider extends LucentDataProvider.Client<ResourceLocation, ItemLighting>
{
	public ItemLightingProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProv)
	{
		super(LucentAssets.ITEM_LIGHTING, output, lookupProv);
	}

	/**
	 * Registers the item lighting from the data passed.
	 * 
	 * @param item
	 *            Used to name the file only
	 * @param itemLighting
	 * @throws NullPointerException
	 *             If the item has no registry name.
	 * @throws IllegalArgumentException
	 *             If a duplicate key is registered.
	 */
	protected void register(Item item, ItemLighting itemLighting) throws NullPointerException, IllegalArgumentException
	{
		ResourceLocation itemID = BuiltInRegistries.ITEM.getKey(item);
		if (itemID == null)
			throw new NullPointerException("The item passed had a null registry name");
		this.register(itemID.getPath(), itemLighting);
	}
}
