package com.legacy.lucent.api.data.providers;

import java.util.concurrent.CompletableFuture;

import com.legacy.lucent.api.data.objects.EmissiveBlockTexture;
import com.legacy.lucent.core.data.managers.LucentAssets;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;

public abstract class EmissiveBlockTexturesProvider extends LucentDataProvider.Client<ResourceLocation, EmissiveBlockTexture>
{
	public EmissiveBlockTexturesProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProv)
	{
		super(LucentAssets.EMISSIVE_BLOCK_TEXTURE, output, lookupProv);
	}

	/**
	 * Registers the emissive texture using the data passed.
	 * 
	 * @param fileName
	 * @param textureName
	 * @param lightLevel
	 * @throws IllegalArgumentException
	 *             If a duplicate key is registered.
	 */
	protected void register(String fileName, ResourceLocation textureName, int lightLevel) throws IllegalArgumentException
	{
		this.register(fileName, new EmissiveBlockTexture(textureName, lightLevel));
	}
}