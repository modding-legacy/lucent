package com.legacy.lucent.api;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.world.entity.Entity;

/**
 * Used to store various {@link VecTransformer}s and the light level that should
 * apply at the position given through the transformer.
 * 
 * @author Silver_David
 *
 */
public class EntityBrightnessMap
{
	private final Map<VecTransformer, Integer> brightnessPoses = new HashMap<>();

	/**
	 * @return The entity to check
	 */
	public Entity getEntity()
	{
		return EntityBrightness.activeEntity;
	}

	public void add(VecTransformer vecTransformer, int light)
	{
		if (light > 0)
			this.brightnessPoses.put(vecTransformer, light);
	}

	public void remove(VecTransformer pos)
	{
		this.brightnessPoses.remove(pos);
	}

	public Map<VecTransformer, Integer> getAll()
	{
		return this.brightnessPoses;
	}
}
