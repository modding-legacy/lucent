package com.legacy.lucent.api.plugin;

import java.util.Collections;
import java.util.List;

import com.legacy.lucent.api.EntityBrightness;
import com.legacy.lucent.api.EntityBrightnessMap;
import com.legacy.lucent.api.registry.ArmorTrimLightingRegistry;
import com.legacy.lucent.api.registry.BlockTextureLightingRegistry;
import com.legacy.lucent.api.registry.EntityLightSourcePosRegistry;
import com.legacy.lucent.api.registry.EntityLightingRegistry;
import com.legacy.lucent.api.registry.ItemLightingRegistry;
import com.legacy.lucent.api.registry.LightLevelProviderRegistry;
import com.legacy.lucent.core.PluginManager;
import com.legacy.lucent.core.plugin.InternalLucentPlugin;

import net.minecraft.client.Minecraft;
import net.minecraft.core.SectionPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Implement this on a class to make it a Lucent plugin. The plugin must be
 * annotated with {@link LucentPlugin}. Lucent will automatically locate and
 * create it.<br>
 * <br>
 * Override the default methods in this interface for Lucent to use them.
 * 
 * @author Silver_David
 * 
 * @see InternalLucentPlugin
 *
 */
@OnlyIn(Dist.CLIENT)
public interface ILucentPlugin extends PluginManager.IBasePlugin
{
	/**
	 * Registers the lighting for an item. BlockItems that emit light and buckets
	 * containing light emiting fluid will automatically be registered as needed.
	 * Use this for special items, like blaze rods
	 * 
	 * @param registry
	 */
	default void registerItemLightings(final ItemLightingRegistry registry)
	{

	}

	/**
	 * Registers the lighting behavior for an entity
	 * 
	 * @param registry
	 */
	default void registerEntityLightings(final EntityLightingRegistry registry)
	{

	}

	/**
	 * Registers the lighting for a block texture
	 * 
	 * @param registry
	 */
	default void registerBlockTextureLightings(final BlockTextureLightingRegistry registry)
	{

	}

	/**
	 * Registers the lighting for armor trim textures
	 * 
	 * @param registry
	 */
	default void registerArmorTrimTextureLightings(final ArmorTrimLightingRegistry registry)
	{

	}

	/**
	 * Registers a function to get the position for where dynamic lighting starts on
	 * an entity. The default position will be the center of the entity
	 * 
	 * @param registry
	 */
	default void registerEntityLightSourcePositionGetter(final EntityLightSourcePosRegistry registry)
	{

	}

	/**
	 * Registers light level provider types for resource packs to be able to use
	 * them properly.
	 * 
	 * @param registry
	 */
	default void registerLightLevelProviderTypes(final LightLevelProviderRegistry registry)
	{

	}

	/**
	 * Used to get the brightness of the entity for dynamic lighting. The final
	 * value will always be the brightest light level that was set for the entity
	 * 
	 * @param entityBrightness
	 */
	default void getEntityLightLevel(final EntityBrightness entityBrightness)
	{

	}

	/**
	 * Used to define additional positions for dynamic lighting. This may
	 * technically be used as a replacement for
	 * {@link #getEntityLightLevel(EntityBrightness)} and
	 * {@link #registerEntityLightSourcePositionGetter(EntityLightSourcePosRegistry)}
	 * 
	 * @param map
	 */
	default void getAdditionalEntityLightLevels(final EntityBrightnessMap map)
	{

	}

	/**
	 * Used to define what items are equipped on an entity. Used to grab items from
	 * the player's armor and hands. Useful for mods that add additional item slots.
	 * 
	 * @param entity
	 * @return A list of equipped items
	 */
	default List<ItemStack> gatherEquippedItems(final Entity entity)
	{
		return Collections.emptyList();
	}

	/**
	 * Fired when a chunk section is flagged for render updates through Lucent. This
	 * will typically be called when something about dynamic lighting changes. This
	 * assumes that the level this happens in is {@link Minecraft#level}.
	 * 
	 * @param sectionPos
	 */
	default void onSectionMarkedDirty(final SectionPos sectionPos)
	{

	}

	/**
	 * Fired when a chunk section should be marked for render updates due to Lucent.
	 * Override this to run your own logic, and return {@code true} to cancel
	 * Lucent's behavior.
	 * 
	 * @param sectionPos
	 * @return True to prevent Lucent from marking the chunk as dirty. Defaults to
	 *         doing nothing and returning false.
	 */
	default boolean markSectionDirty(final SectionPos sectionPos)
	{
		return false;
	}
}
