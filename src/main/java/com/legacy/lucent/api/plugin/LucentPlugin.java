package com.legacy.lucent.api.plugin;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

/**
 * Annotate a class with this to mark it as a plugin. The plugin must implement
 * {@link ILucentPlugin}. Lucent will automatically locate and create it.
 * 
 * @author Silver_David
 *
 */
@OnlyIn(Dist.CLIENT)
public @interface LucentPlugin
{

}
