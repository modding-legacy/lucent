Used to register entity lighting. Configs will always override this.

## Data
- `id`: The registry name of the entity to use.
- `light`: The amount of light the entity should emit (0 - 15).

## Example
```json
{
	"id": "minecraft:creeper",
	"light": 9
}
```