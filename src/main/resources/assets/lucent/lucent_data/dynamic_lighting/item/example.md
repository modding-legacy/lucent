Used to register item lighting. Configs will always override this.

## Data
- `id`: The registry name of the item to use.
- `light`: The amount of light the item should emit (0 - 15).

## Example
```json
{
	"id": "minecraft:diamond",
	"light": 15
}
```