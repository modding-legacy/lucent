Used to register emissive lighting to block textures in the world. Configs will always override this.

## Data
- `texture`: The full name of the texture to use.
- `light`: The amount of light the texture should emit (0 - 15).

## Example
```json
{
	"texture": "minecraft:block/sand",
	"light": 13
}
```