# 1.20.4-v1.7.0

## General Lighting Updates
- Falling blocks, block display, and item display entities can now emit light based on the block or item they represent.

## API Updates
- Added `List<ItemStack> ILucentPlugin.gatherEquippedItems(Entity)`
	- Return a list of all equipped items on the entity that could determine dynamic lighting.
	- Internally used to handle armor, held items, and item frames. Other mods could use this to handle additional inventory slots.