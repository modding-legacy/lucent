<h1 align="center">
<img src="src/main/resources/banner.png" alt="Lucent">
</h1>

## About

Lucent is a dynamic lighting mod and API designed with performance and configurability in mind. It's main features include:
- Dynamic lighting for entities:
    - Entities on fire or with the glowing effect will emit light.
        - These can be disabled in the config.
    - Entities can be set to glow through the use of `ILucentPlugin` or the config file.
- Dynamic lighting for items:
    - This affects items in the world, equipped on an entity, or in an item frame.
    - Items that place blocks will emit a light level equal to their respective block's light.
    - Items can be registered to glow through the use of `ILucentPlugin` or the config file.
- Light blending:
    - By default, this mod tries to smooth lighting when moving by assigning light values based on the exact distance a block is from the source of the light.
    - Can be disabled in the config to improve performance.
- Settings to control whether or not an entity should emit light based on distance and visibility:
    - If an entity is not visible, not exposed to the sun, and further than 24 blocks away, it won't emit light.
        - Can be configured to always emit light.
    - If an entity is too far away, 128 blocks by default, it won't emit light.
        - The distance with which entities are allowed to emit light can be configured. 
- Controllable refresh rate:
    - Dynamic lighting updates only happen according to the refresh rate set in the config. 
    - Lighting can be configured to refresh between 1 and 20 times a second.
- Emissive rendering for blocks in the world:
    - This works on a per-texture basis. 
        - IE: setting "block/glass" to be emissive will make glass and glass panes emissive since they share the same texture.
    - Textures can be set to emit light through the use of `ILucentPlugin` or the config file.

## Compatibility

Lucent natively includes support support for the following mods: 
- Blue Skies
- Create
- Lava Monster
- Nethercraft
- Quark

Any mods not supported will have the following items emit light:
- Items that place light emitting blocks.
- Buckets filled with a fluid that emits light.
- Burning entities.
- Entities with the glowing effect.

Adding Lucent compatibility to your mod:
- Intall the project from our maven into your workspace and use the included `ILucentPlugin` interface with the `@LucentPlugin` annotation on a class. Lucent will automatically detect the class as a plugin and use it.
- Resource packs can support basic functionality, similar to what can be done in the config.
    - Lucent resource pack data exists in `assets/<yourmod>/lucent/`.
    - Check the source code for [examples](https://gitlab.com/modding-legacy/lucent/-/tree/1.18.x/src/main/resources/assets/lucent/lucent).

## How it Works

Lucent works by internally storing data about block positions and how much light they should emit. This value is then used when Minecraft's renderer tries to get the light at a given position. Smoothly blending light is done by adjusting the light value at a position based on its distance from the source.

The system that controls where dynamic lighting exists runs in a separate thread to reduce it's impact on FPS. This thread executes according to the refresh rate set in the config with a maximum speed of 20 executions per second.

When the dynamic lighting in a chunk section is changed, the section will be queued to update. By default, this update happens on a separate thread from the main client thread, minimizing impact on FPS. Since this may cause weird rendering glitches, this function can be disabled in the config.

## Installing in Your Workspace

Lucent can be downloaded from the Modding Legacy maven repository via the buildscript. To do so, follow these steps:

1. In the `build.gradle` file for your mod, add the Modding Legacy maven repository to the `repositories` block (it is not in the `buildscript` block)

```groovy
repositories {
    // ...

    maven {
        name "Modding Legacy Maven"
        url "https://maven.moddinglegacy.com/artifactory/modding-legacy/"
    }
}
```

2. In the `build.gradle` file for your mod, add Lucent as a dependency. Replace `implementation` with `runtimeOnly` if this is an optional dependency.

```groovy
dependencies {
    // ...

    implementation fg.deobf("com.legacy:lucent:1.18.1-1.2.0")
}
```

To view a list of all the available versions, go to [our maven](https://maven.moddinglegacy.com/service/rest/repository/browse/modding-legacy/com/legacy/lucent/) to browse the files.

3. Add `all` to your `runs` so that Mixins use the proper remapped names. Using the `all` run will automatically put it in all other run types.

```groovy
minecraft {
    runs {
        // ...

        all {
            def srgToMcpFile = project.tasks.createSrgToMcp.outputs.files[0].path

            property 'net.minecraftforge.gradle.GradleStart.srg.srg-mcp', srgToMcpFile
            property 'mixin.env.remapRefMap', 'true'
            property 'mixin.env.refMapRemappingFile', srgToMcpFile
        }
        
        // ...
    }
}
```

4. Add Lucent as a dependency for your mod in its `mods.toml` file if it should be required.

```toml
[[dependencies.examplemod]]
    modId="lucent"
    mandatory=true
    versionRange="[1.2.0,]"
    ordering="NONE"
    side="CLIENT"
```

5. Setup your workspace with the appropriate gradlew commands for your IDE.
    - For Eclipse
        - ``gradlew eclispe``
        - ``gradlew genEclipseRuns``
    - For IntelliJ
        - ``gradlew genIntellijRuns``

6. From here, assuming you see Lucent in your project's external dependencies, you should be able to use the API. If you do not see it, try refreshing your project as some IDEs won't do that automatically.
