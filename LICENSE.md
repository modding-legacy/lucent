**[API Package](https://gitlab.com/modding-legacy/lucent/-/tree/1.19.x/src/main/java/com/legacy/lucent/api), [assets folder](https://gitlab.com/modding-legacy/lucent/-/tree/1.19.x/src/main/resources/assets), and [examples](https://gitlab.com/modding-legacy/lucent/-/tree/1.19.x/examples)**: [lgpl-3.0](https://www.gnu.org/licenses/lgpl-3.0.en.html)

**Otherwise**: [ARR](https://moddinglegacy.com/ML-General-Terms/)
